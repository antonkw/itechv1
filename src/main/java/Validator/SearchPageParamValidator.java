package Validator;

import Contact.*;
import DBService.JDBCContactsDAO;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Created by antonkw on 11.03.2015.
 */
public class SearchPageParamValidator implements ParametersValidator {
    private HttpServletRequest request;
    private JDBCContactsDAO jdbcContactsDAO = null;
    private SearchRequest searchRequest = null;
    private int countPerPage;
    private int countOfPages;
    private int pageNumber;
    int startPosition;

    public SearchPageParamValidator(HttpServletRequest request) throws Exception {
        this.request = request;
        if (request.getAttribute("searchRequest") == null) {
            throw new Exception("search request absent!");
        }
        searchRequest = (SearchRequest) request.getAttribute("searchRequest");

        setCountPerPage();
    }

    public void setContactList() {
        jdbcContactsDAO = new JDBCContactsDAO();
        ArrayList<Contact> contacts =  jdbcContactsDAO.getSomeContactsForRequest(startPosition, countPerPage, searchRequest);
        System.out.println(searchRequest.getName());
        request.setAttribute("contacts", contacts);

    }


    public void setCountPerPage() {
        countPerPage = 10;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("count")) {
                    countPerPage = Integer.parseInt(cookie.getValue());
                }
            }
        }
    }

    @Override
    public int getCountOfPages() {
        jdbcContactsDAO = new JDBCContactsDAO();
        countOfPages = (int) Math.ceil((double)jdbcContactsDAO.getCountOfRequestResult(searchRequest)/countPerPage);
        return countOfPages;
    }

    @Override
    public int getPageNumber() {
        pageNumber = 1;
        if (request.getParameter("page") != null){
            boolean canParse = true;
            String tryPageString = request.getParameter("page");
            char[] symbols = tryPageString.toCharArray();
            String validationString = "0123456789";
            for(char c : symbols){
                if(validationString.indexOf(c)==-1) canParse = false;
            }
            if (canParse)
            {
                int tryPageNumber = Integer.parseInt(tryPageString);
                if (tryPageNumber > 0 && tryPageNumber <= countOfPages)
                {
                    pageNumber = tryPageNumber;
                }
            }
        }
        setStartPosition();
        return pageNumber;
    }

    public void setStartPosition() {
        startPosition = pageNumber * countPerPage - countPerPage;
    }
}
