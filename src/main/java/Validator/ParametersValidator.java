package Validator;

/**
 * Created by antonkw on 20.02.2015.
 */
public interface ParametersValidator {
    public int getPageNumber();
    public int getCountOfPages();
}
