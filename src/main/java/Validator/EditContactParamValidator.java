package Validator;

import Contact.Contact;
import DBService.JDBCContactsDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

/**
 * Created by antonkw on 09.03.2015.
 */
public class EditContactParamValidator {
    Logger log = LogManager.getLogger(EditContactParamValidator.class);

    public void setContactForEdit(HttpServletRequest request) throws Exception {
        if (request.getParameterMap().containsKey("id_of_edit_contact")) {
            boolean canParse = true;
            request.setAttribute("context", "edit");
            String tryIdString = request.getParameter("id_of_edit_contact");
            char[] symbols = tryIdString.toCharArray();
            String validationString = "0123456789";
            for (char c : symbols) {
                if (validationString.indexOf(c) == -1) canParse = false;
            }
            if (canParse) {
                int tryIdNumber = Integer.parseInt(tryIdString);
                if (tryIdNumber < 1) {
                    throw new ServletException("incorrect id (<0)");
                }
                JDBCContactsDAO jdbcContactsDAO = new JDBCContactsDAO();
                Contact contact = null;
                try {
                    contact = jdbcContactsDAO.getFullContact(tryIdNumber);
                    request.setAttribute("contact", contact);
                } catch (SQLException e) {
                    log.error(e);
                    throw e;
                }

            } else {
                throw new ServletException("incorrect id (<0)");
            }
        } else {
            throw new ServletException("no id for edit!");
        }
    }
}
