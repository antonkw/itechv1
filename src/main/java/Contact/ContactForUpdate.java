package Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonkw on 08.03.2015.
 */
public class ContactForUpdate extends Contact {
    private List<Integer> phonesToDelete = new ArrayList<>();
    private List<Integer> filesToDelete = new ArrayList<>();
    private List<Phone> phonesToUpdate = new ArrayList<>();
    private List<AttachmentFile> filesToUpdate = new ArrayList<>();

    public List<Integer> getPhonesToDelete() {
        return phonesToDelete;
    }

    public void setPhonesToDelete(List<Integer> phoneToDelete) {
        this.phonesToDelete = phoneToDelete;
    }

    public List<Integer> getFilesToDelete() {
        return filesToDelete;
    }

    public void setFilesToDelete(List<Integer> filesToDelete) {
        this.filesToDelete = filesToDelete;
    }

    public List<Phone> getPhonesToUpdate() {
        return phonesToUpdate;
    }

    public void setPhonesToUpdate(List<Phone> phonesToUpdate) {
        this.phonesToUpdate = phonesToUpdate;
    }

    public List<AttachmentFile> getFilesToUpdate() {
        return filesToUpdate;
    }

    public void setFilesToUpdate(List<AttachmentFile> filesToUpdate) {
        this.filesToUpdate = filesToUpdate;
    }
}
