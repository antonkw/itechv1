package Contact;

import java.util.ArrayList;

/**
 * Created by antonkw on 16.02.2015.
 */
public class ContactsContainer {
    private ArrayList<Contact> contactArrayList = new ArrayList<Contact>();
    private int position;

    public ContactsContainer() {
        position = -1;
    }

    public void addContact(Contact contact) {
        contactArrayList.add(contact);
    }
    public ContactsContainer(int firstPosition, int lastPosition) {
        position = 0;
    }

    public void loadFromSQL(int firstPosition, int lastPosition) {

    }

    public boolean hasNext() {

        return position != contactArrayList.size() - 1;
    }

    public Contact getNext() {
        ++position;
        return contactArrayList.get(position);
    }
}
