package Contact;

import org.joda.time.DateTime;

/**
 * Created by antonkw on 02.03.2015.
 */
public class AttachmentFile {
    private int fileId;
    private String fileName;
    private int personId;
    private DateTime dateOfUpload;
    private String fileUserName;
    private String comment;


    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public DateTime getDateOfUpload() {
        return dateOfUpload;
    }

    public void setDateOfUpload(DateTime dateOfUpload) {
        this.dateOfUpload = dateOfUpload;
    }

    public String getFileUserName() {
        return fileUserName;
    }

    public void setFileUserName(String fileUserName) {
        this.fileUserName = fileUserName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
