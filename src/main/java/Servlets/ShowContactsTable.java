package Servlets;

import Contact.Contact;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by antonkw on 16.02.2015.
 */

public class ShowContactsTable extends HttpServlet {
    Logger log = LogManager.getLogger(ShowContactsTable.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        drowTable(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        drowTable(request,response);
    }

    protected void drowTable(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        if (request.getAttribute("contacts") == null)
        {
            log.error("contacts attribute absent");
            throw new ServletException("no contacts!");
        }
        contacts = (ArrayList<Contact>) request.getAttribute("contacts");

        log.info("size of contacts list: {}", contacts.size());
        PrintWriter outLine = response.getWriter();
        outLine.printf("%s%s", "<table id=\"hor-minimalist-b\">\n<tr>\n<th>\n</th>\n<th>\nFull Name\n</th>\n<th>\nDate of birth\n",
                "</th>\n<th>\nAddress\n</th>\n<th>\nCurrent place of work\n</th>\n</tr>\n");
        for (Contact contact : contacts) {
            if (contact.getPatronymic() != null) {
                outLine.printf("<tr>\n<td>\n<input type=\"checkbox\" id=\"box%d\" name=\"personId\" value=\"%d\"\n></td>\n<td>\n<a href=\"frontServlet?command=Edit&id_of_edit_contact=%d\">%s %s %s</a></td>\n",
                        contact.getPersonId(), contact.getPersonId(), contact.getPersonId(), contact.getLastName(), contact.getName(), contact.getPatronymic());
            } else {
                outLine.printf("<tr>\n<td>\n<input type=\"checkbox\" id=\"box%d\" name=\"personId\" value=\"%d\"\n></td>\n<td>\n<a href=\"frontServlet?command=Edit&id_of_edit_contact=%d\">%s %s</a></td>\n",
                        contact.getPersonId(), contact.getPersonId(), contact.getPersonId(), contact.getLastName(), contact.getName());

            }
            if (contact.getDate() != null) {
                outLine.printf("<td>\n%s</td>\n", contact.getDate());
            } else {
                outLine.print("<td>\n</td>\n");
            }
            StringBuilder stringBuilder = new StringBuilder();
            boolean startIndex = true;
            if (contact.getCountry() != null) {
                stringBuilder.append(contact.getCountry());
                startIndex = false;
            }
            if (contact.getCity() != null) {
                if (!startIndex) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(contact.getCity());
            }
            if (contact.getLocation() != null) {
                if (!startIndex) {
                    stringBuilder.append(", ");
                    stringBuilder.append(contact.getLocation());
                }
            }
            outLine.printf("<td>\n%s\n</td>\n", stringBuilder);
            if (contact.getWork() != null) {
                outLine.printf("<td>\n%s\n</td>\n", contact.getWork());
            } else {
                outLine.print("<td>\n</td>\n");
            }
            outLine.print("</tr>\n");
        }
        outLine.print("</table>\n");

    }
}
