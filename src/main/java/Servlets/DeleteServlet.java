package Servlets;

import DBService.JDBCContactsDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by antonkw on 10.03.2015.
 */
public class DeleteServlet extends HttpServlet {
    Logger log = LogManager.getLogger(DeleteServlet.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter outLine = response.getWriter();
        if (StringUtils.isEmpty((String) request.getAttribute("contactsToDelete"))) {
            outLine.print("you didn`t checked anything!");
        }

        StringTokenizer st = new StringTokenizer((String)request.getAttribute("contactsToDelete"));
        List <Integer> contactsToDelete = new ArrayList<Integer>();
        JDBCContactsDAO jdbcContactsDAO = new JDBCContactsDAO();
        while (st.hasMoreTokens()) {
            contactsToDelete.add(Integer.parseInt(st.nextToken()));
        }
        try {
            jdbcContactsDAO.removeContacts(contactsToDelete);
            response.sendRedirect("index.jsp");
        }
        catch (Exception e) {
            outLine.print("no such id!");
            log.error(e);
        }

    }
}
