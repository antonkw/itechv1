package Servlets;

import Contact.*;
import DBService.JDBCContactsDAO;
import DBService.JDBCFilesDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.*;


/**
 * Created by antonkw on 22.02.2015.
 */
@MultipartConfig
public class AddContact extends HttpServlet {
    Logger log = LogManager.getLogger(AddContact.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean phonesAddError = false;    //если не пришло, какое-либо из значений, обеспечивающих целостность данных, то в БД запись добавлять не будет, юзеру выведется ошибка
                                            //по сути, такое возомжно только в случае обрыва связи или же если кто-то целенаправленно будет отсылать вручную составленные некорректные запросы
        PrintWriter out = response.getWriter();
        request.setCharacterEncoding("UTF-8");
        if (!ServletFileUpload.isMultipartContent(request)) {
            PrintWriter writer = response.getWriter();
            writer.println("Error: Form must has enctype=multipart/form-data.");
            writer.flush();
            return;
        }
        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // sets memory threshold - beyond which files are stored in disk
        factory.setSizeThreshold(50000000);
        // sets temporary location to store files
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        // sets maximum size of upload file
        upload.setFileSizeMax(5000000);
        // sets maximum size of request (include file + form data)
        upload.setSizeMax(5000000);
        // constructs the directory path to store upload file
        // this path is relative to application's directory
        String uploadPath = getServletContext().getRealPath("")
                + File.separator + "upload";
        // creates the directory if it does not exist
        Map <String, String> params = new HashMap<String, String>();
        List<FileItem> formItems = null;

        try {
            // parses the request's content to extract file data

            formItems = upload.parseRequest(request);
            if (formItems != null & formItems.size() > 0) {
                // iterates over form's fields
                for (FileItem item : formItems) {
                    if (item.isFormField()) {
                        // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                        String fieldName = item.getFieldName();
                        InputStream stream = item.getInputStream();
                        params.put(fieldName, Streams.asString(stream, "UTF-8"));
                    }
                }
            }
        } catch (Exception ex) {
            request.setAttribute("message", "There was an error: " + ex.getMessage());
            log.error(ex);
        }
        Contact contact = new Contact();
        if (StringUtils.isEmpty(params.get("lastName"))) {
            out.println("invalid lastname");
            return;
        }
        contact.setLastName(params.get("lastName"));
        if (StringUtils.isEmpty(params.get("name"))) {
            out.println("invalid name");
            return;
        }
        contact.setName(params.get("name"));
        if (StringUtils.isEmpty(params.get("email"))) {
            out.println("invalid mail");
            return;
        }
        contact.setEmail(params.get("email"));
        if (!StringUtils.isEmpty(params.get("patronymic"))) {
            contact.setPatronymic(params.get("patronymic"));
        }
        if (!StringUtils.isEmpty(params.get("dateOfBirth"))) {
            contact.setDate(params.get("dateOfBirth"));
            System.out.println("date: " + contact.getDate());
        }
        if (!StringUtils.isEmpty(params.get("citizenship"))) {
            contact.setCountryOfCitizenship(params.get("citizenship"));
        }
        if (!StringUtils.isEmpty(params.get("sex"))) {
            contact.setSex(params.get("sex"));
        }
        if (!StringUtils.isEmpty(params.get("maritalStatus"))) {
            contact.setMaritalStatus(params.get("maritalStatus"));
        }
        if (!StringUtils.isEmpty(params.get("website"))) {
            contact.setSite(params.get("website"));
        }
        if (!StringUtils.isEmpty(params.get("email"))) {
            contact.setEmail(params.get("email"));
        }
        if (!StringUtils.isEmpty(params.get("work"))) {
            contact.setWork(params.get("work"));
        }
        if (!StringUtils.isEmpty(params.get("country"))) {
            contact.setCountry(params.get("country"));
        }
        if (!StringUtils.isEmpty(params.get("city"))) {
            contact.setCity(params.get("city"));
        }
        if (!StringUtils.isEmpty(params.get("house"))) {
            contact.setLocation(params.get("house"));
        }
        if (!StringUtils.isEmpty(params.get("zipCode"))) {
            contact.setZipcode(Integer.parseInt(params.get("zipCode")));
        }
        int countOfPhones = 0;
        try {
            countOfPhones = Integer.parseInt(params.get("countOfPhones"));
        }
        catch (NullPointerException e)
        {
            log.error(e);
        }
        List<Phone> phones = new ArrayList<Phone>();
        int i = 1;
        Phone phone = null;
        while(i <= countOfPhones){//какой-то разумный лимит


            if(params.containsKey("phoneNumber" + i)){
                phone = new Phone();
                phone.setPhone(Integer.parseInt(params.get("phoneNumber" + i)));
                System.out.println(phone.getPhone());
            }
            else {
                ++i;
                continue;

            }
            if(params.containsKey("operatorCode" + i)){
                phone.setOperatorCode(Integer.parseInt(params.get("operatorCode" + i)));
                System.out.println("oper code: " + phone.getOperatorCode());
            }
            else {
                phonesAddError = true;
            }
            if(params.containsKey("countryCode" + i)){
                phone.setCountryCode(Integer.parseInt(params.get("countryCode" + i)));
                System.out.println("country code: " + phone.getCountryCode());
            }
            else {
                phonesAddError = true;
            }
            if(params.containsKey("phoneType" + i)){
                phone.setType(params.get("phoneType" + i));
                System.out.println("phone type: " + phone.getType());
            }
            else {
                phonesAddError = true;
            }
            if(params.containsKey("comment" + i)){
                phone.setComment(params.get("comment" + i));
                System.out.println("comment: " + phone.getComment());
            }
            else {
                phonesAddError = true;
            }
            phones.add(phone);
            System.out.println("size of phones: " + phones.size());


            ++i;

        }
        contact.setPhones(phones);
        JDBCContactsDAO jdbcContactsDAO = new JDBCContactsDAO();

        try {

            jdbcContactsDAO.getConnection();
            int idOfAddedContact;
            jdbcContactsDAO.startTransaction();
            idOfAddedContact = jdbcContactsDAO.insert(contact);
                if (formItems != null & formItems.size() > 0) {
                    File uploadDir = new File(uploadPath);
                    if (!uploadDir.exists()) {
                        uploadDir.mkdir();
                    }
                    JDBCFilesDAO jdbcFilesDAO = new JDBCFilesDAO(jdbcContactsDAO.getConnection());
                    // iterates over form's fields
                    for (FileItem item : formItems) {
                        if (!item.isFormField()) {
                            if (!StringUtils.isEmpty(new File(item.getName()).getName())) {
                                String uploadUserPath = uploadPath + File.separator + idOfAddedContact;

                                File uploadUserDir = new File(uploadUserPath);
                                if (!uploadUserDir.exists()) {
                                    uploadUserDir.mkdir();
                                }

                                if (!"0".equals(item.getFieldName())) {
                                    System.out.print(item.getFieldName());


                                    File file = returnFinalFile(uploadUserPath, item);


                                    item.write(file);

                                    AttachmentFile attachmentFile = new AttachmentFile();
                                    int fileTableNumber = Integer.parseInt(item.getFieldName());
                                    if (params.containsKey("file-user-name-" + fileTableNumber)) {
                                        attachmentFile.setFileUserName(params.get("file-user-name-" + fileTableNumber));

                                    } else {
                                        SQLException e = new SQLException("no id number");
                                        log.error(e);
                                        throw(e);

                                    }
                                    if (params.containsKey("fileComment" + fileTableNumber)) {
                                        attachmentFile.setComment(params.get("fileComment" + fileTableNumber));
                                    } else {
                                        SQLException e = new SQLException("no file comment");
                                        log.error(e);
                                        throw(e);
                                    }
                                    attachmentFile.setPersonId(idOfAddedContact);
                                    attachmentFile.setDateOfUpload(DateTime.now(DateTimeZone.UTC));
                                    attachmentFile.setFileName(file.getName());
                                    jdbcFilesDAO.insert(attachmentFile);
                                    request.setAttribute("message", "Upload has been done successfully!");
                                    out.println("Upload has been done successfully!");
                                } else {
                                    try (InputStream input = item.getInputStream()) {
                                        try {
                                            if (ImageIO.read(input) != null) {
                                                String uploadUserAvatarPath = uploadPath + File.separator + contact.getPersonId() + File.separator + "avatar";

                                                uploadUserDir = new File(uploadUserAvatarPath);
                                                if (!uploadUserDir.exists()) {
                                                    uploadUserDir.mkdir();
                                                } else {
                                                    for (File f : uploadUserDir.listFiles()) {
                                                        f.delete();
                                                    }
                                                }
                                                File file = returnFinalFile(uploadUserAvatarPath, item);
                                                contact.setAvatar(file.getName());
                                                item.write(file);
                                                request.setAttribute("message", "Upload has been done successfully!");
                                                out.println("Upload has been done successfully!");
                                            }

                                        } catch (Exception e) {
                                            log.error(e);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            jdbcContactsDAO.commitTransaction();
            response.sendRedirect("index.jsp");

        } catch (Exception e) {
            jdbcContactsDAO.rollbackTransaction();
            log.error(e);

        } finally {
            jdbcContactsDAO.closeConnection();
        }

}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private File returnFinalFile(String uploadUserPath, FileItem fileItem) {
        UUID uid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
        String fileName = uid.randomUUID() + "." + FilenameUtils.getExtension(new File(fileItem.getName()).getName());
        File uploadFile = new File(uploadUserPath + File.separator + fileName);

        while (uploadFile.exists()) {
            fileName = uid.randomUUID() + "." + FilenameUtils.getExtension(new File(fileItem.getName()).getName());
            uploadFile = new File(uploadUserPath + File.separator + fileName);
        }
        return uploadFile;

    }


}
