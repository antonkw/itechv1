package Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by antonkw on 16.02.2015.
 */
public class ActionFilter extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("buttonName").equals("deleteButton")) {
            HttpSession session = request.getSession(false);
            session.setAttribute("contactsToDelete", request.getParameter("contactsToDelete"));
            response.sendRedirect("./frontServlet?command=Delete");
        }
        if (request.getParameter("buttonName").equals("spamButton")){
            System.out.println(request.getParameterNames());
            HttpSession session = request.getSession(false);
            session.setAttribute("contactsToSpam", request.getParameter("contactsToDelete"));

            response.sendRedirect("./frontServlet?command=Spam");

        }
        if (request.getParameter("buttonName").equals("createButton"))
            response.sendRedirect("./frontServlet?command=Create");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
