package Servlets;

import Contact.SearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by antonkw on 11.03.2015.
 */
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        SearchRequest searchRequest = new SearchRequest();

        if (StringUtils.isEmpty(request.getParameter("name"))) {
            searchRequest.setName(request.getParameter("name"));
        }
        if (StringUtils.isEmpty(request.getParameter("lastName"))) {
            searchRequest.setName(request.getParameter("lastName"));
        }
        if (StringUtils.isEmpty(request.getParameter("patronymic"))) {
            searchRequest.setPatronymic(request.getParameter("patronymic"));
        }
        if (StringUtils.isEmpty(request.getParameter("dateOfBirth"))) {
            searchRequest.setDateTime(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(request.getParameter("dateOfBirth")));

        }
        if (StringUtils.isEmpty(request.getParameter("citizenship"))) {
            searchRequest.setCountryOfCitizenship(request.getParameter("citizenship"));
        }
        if (!StringUtils.isEmpty(request.getParameter("sex"))) {
            searchRequest.setSex(request.getParameter("sex"));
        }
        if (!StringUtils.isEmpty(request.getParameter("maritalStatus"))) {
            searchRequest.setMaritalStatus(request.getParameter("maritalStatus"));
        }
        if (!StringUtils.isEmpty(request.getParameter("work"))) {
            searchRequest.setWork(request.getParameter("work"));
        }
        if (!StringUtils.isEmpty(request.getParameter("country"))) {
            searchRequest.setCountry(request.getParameter("country"));
        }
        if (!StringUtils.isEmpty(request.getParameter("city"))) {
            searchRequest.setCity(request.getParameter("city"));
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
