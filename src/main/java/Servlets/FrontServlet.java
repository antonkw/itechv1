package Servlets;

import Contact.SearchRequest;
import Validator.SearchPageParamValidator;
import Validator.ShowPageParamValidator;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by antonkw on 23.02.2015.
 */
public class FrontServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        FrontCommand command = null;
        try {
            command = getCommand(request);
            command.init(getServletContext(), request, response);
            command.process();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.setCharacterEncoding("UTF-8");
        FrontCommand command = null;
        try {
            command = getCommand(request);
            command.init(getServletContext(), request, response);
            command.process();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private FrontCommand getCommand(HttpServletRequest request) throws Exception {
        try {
            return (FrontCommand) getCommandClass(request).newInstance();
        } catch (Exception e) {
            throw new Exception(e);

        }
    }

    private Class getCommandClass(HttpServletRequest request) {
        Class result;
        final String commandClassName =
                "Servlets." + (String) request.getParameter("command") + "Command";
        try {
            result = Class.forName(commandClassName);
        } catch (ClassNotFoundException e) {
            result = UnknownCommand.class;
        }
        return result;
    }
}

abstract class FrontCommand {
    protected ServletContext context;
    protected HttpServletRequest request;
    protected HttpServletResponse response;

    public void init(ServletContext context,
                     HttpServletRequest request,
                     HttpServletResponse response) {
        this.context = context;
        this.request = request;
        this.response = response;
    }

    abstract public void process() throws Exception;

    protected void forward(String target) throws ServletException, IOException {
        RequestDispatcher dispatcher = context.getRequestDispatcher(target);
        dispatcher.forward(request, response);
    }
}

class ArtistCommand extends FrontCommand {
    public void process() throws ServletException, IOException {

        forward("/artist.jsp");
    }

}

class UnknownCommand extends FrontCommand {
    public void process() throws ServletException, IOException {
        forward("/unknown.jsp");
    }
}

class EditCommand extends FrontCommand {
    public void process() throws ServletException, IOException {
        forward("/editContact.jsp");
    }


}

class ShowCommand extends FrontCommand {
    public void process() throws ServletException, IOException {

        ShowPageParamValidator showPageParamValidator = new ShowPageParamValidator(request);
        Integer countOfPages = showPageParamValidator.getCountOfPages();
        Integer pageNumber = showPageParamValidator.getPageNumber();
        request.setAttribute("pageNumber", pageNumber);
        request.setAttribute("countOfPages", countOfPages);
        showPageParamValidator.setContactList();
        forward("/showContacts.jsp");
    }
}

class MailCommand extends FrontCommand {
    public void process() throws ServletException, IOException {

        forward("/mail");
    }
}

class StartSearchCommand extends FrontCommand {
    public void process() throws ServletException, IOException {

        forward("/search.jsp");
    }
}

class SearchCommand extends FrontCommand {
    public void process() throws Exception {
        SearchRequest searchRequest = null;




        searchRequest = new SearchRequest();

        if (!StringUtils.isEmpty(request.getParameter("name"))) {
            searchRequest.setName(request.getParameter("name"));
        }

        if (!StringUtils.isEmpty(request.getParameter("lastName"))) {
            searchRequest.setLastName(request.getParameter("lastName"));
        }
        if (!StringUtils.isEmpty(request.getParameter("patronymic"))) {
            searchRequest.setPatronymic(request.getParameter("patronymic"));
        }
        if (!StringUtils.isEmpty(request.getParameter("dateOfBirth"))) {
            searchRequest.setDateTime(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(request.getParameter("dateOfBirth")));
        }
        if (!StringUtils.isEmpty(request.getParameter("date"))) {
            if ("after".equals(request.getParameter("date"))) {
                searchRequest.setLater(false);
            } else if ("before".equals(request.getParameter("date"))) {
                searchRequest.setLater(true);
            }
        }
        if (!StringUtils.isEmpty(request.getParameter("citizenship"))) {
            searchRequest.setCountryOfCitizenship(request.getParameter("citizenship"));
        }
        if (!StringUtils.isEmpty(request.getParameter("sex"))) {
            searchRequest.setSex(request.getParameter("sex"));
        }
        if (!StringUtils.isEmpty(request.getParameter("maritalStatus"))) {
            searchRequest.setMaritalStatus(request.getParameter("maritalStatus"));
        }
        if (!StringUtils.isEmpty(request.getParameter("work"))) {
            searchRequest.setWork(request.getParameter("work"));
        }
        if (!StringUtils.isEmpty(request.getParameter("country"))) {
            searchRequest.setCountry(request.getParameter("country"));
        }
        if (!StringUtils.isEmpty(request.getParameter("city"))) {
            searchRequest.setCity(request.getParameter("city"));

        }

        request.setAttribute("searchRequest", searchRequest);




        SearchPageParamValidator showPageParamValidator = new SearchPageParamValidator(request);
        Integer countOfPages = showPageParamValidator.getCountOfPages();
        Integer pageNumber = showPageParamValidator.getPageNumber();
        request.setAttribute("pageNumber", pageNumber);
        request.setAttribute("countOfPages", countOfPages);
        showPageParamValidator.setContactList();
        forward("/showSearch.jsp");
    }
}


class CreateCommand extends FrontCommand {
    public void process() throws ServletException, IOException {
        forward("/addContact.jsp");
    }
}

class SpamCommand extends FrontCommand {
    public void process() throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("contactsToSpam", session.getAttribute("contactsToSpam"));
        session.removeAttribute("contactsToSpam");

        forward("/mail.jsp");
    }
}

class DeleteCommand extends FrontCommand {
    public void process() throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("contactsToDelete", session.getAttribute("contactsToDelete"));
        session.removeAttribute("contactsToDelete");
        forward("/delete");
    }
}
