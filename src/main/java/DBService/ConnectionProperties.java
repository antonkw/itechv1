package DBService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by antonkw on 17.02.2015.
 */
public class ConnectionProperties {
    Logger log = LogManager.getLogger(ConnectionProperties.class);
    private static ConnectionProperties instance;
    private String url;
    private String driver;
    private String user;
    private String pass;

    private ConnectionProperties() throws Exception {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            String propFileName = "db_config.properties";
            input = getClass().getClassLoader().getResourceAsStream(propFileName);

            if(input == null){
                throw new Exception("cannot read props" + getClass().getClassLoader().getResource(propFileName).getPath());
            }

            prop.load(input);
            url = prop.getProperty("database");
            driver = prop.getProperty("driver");
            user = prop.getProperty("user");
            pass = prop.getProperty("pass");


        } catch (IOException ex) {
            log.error("property reading exeption", ex);
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }
    }

    public static synchronized ConnectionProperties getInstance() throws Exception {
        if (instance == null) {
            instance = new ConnectionProperties();
        }
        return instance;
    }

    public String getUrl() {
        return url;
    }

    public String getDriver() {
        return driver;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }
}
