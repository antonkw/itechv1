package DBService;

import Contact.AttachmentFile;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created by antonkw on 03.03.2015.
 */
public class AttachmentsDAO extends TemplateDAO {

    public AttachmentsDAO(Connection connection) {
        this.connection = connection;
    }

    public void insert(AttachmentFile file) {
        try {
            checkConnection("file attachment");
            statement = connection.prepareStatement("INSERT INTO FILES (file_name, file_location, person_id, date_of_upload) VALUES(?, ?, ?, ?)");
            statement.setString(1,file.getFileUserName());
            statement.setString(2,file.getFileName());
            statement.setInt(3,file.getPersonId());
            statement.setTimestamp(4, new Timestamp(file.getDateOfUpload().getMillis()));
            statement.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();

        }
        finally {
            closeStatement();
        }
    }
}
