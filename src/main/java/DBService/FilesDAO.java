package DBService;

import Contact.AttachmentFile;

import java.sql.SQLException;

/**
 * Created by antonkw on 02.03.2015.
 */
public interface FilesDAO {
    public void insert(AttachmentFile file) throws Exception;
}
