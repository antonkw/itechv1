package DBService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by antonkw on 03.03.2015.
 */
public class TemplateDAO {
    protected Connection connection = null;
    protected PreparedStatement statement = null;
    Logger log = LogManager.getLogger(PhonesDAO.class);

    protected void checkConnection(String s) throws SQLException {
        if (connection == null) {
            StringBuilder error = new StringBuilder("no connection before ");
            error.append(s);
            SQLException e = new SQLException(error.toString());
            log.error(e);
            throw(e);
        }
    }

    protected void closeStatement() {
        if (statement != null)        {
            try {
                statement.close();
            } catch (SQLException sqle) {
                log.error(sqle);
            }
        }
    }
}
