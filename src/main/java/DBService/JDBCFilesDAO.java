package DBService;

import Contact.AttachmentFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonkw on 02.03.2015.
 */
public class JDBCFilesDAO extends TemplateDAO implements FilesDAO {

    Logger log = LogManager.getLogger(JDBCFilesDAO.class);

    public JDBCFilesDAO (Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() throws Exception {
        log.info("get connection");
        try {
            Class.forName(ConnectionProperties.getInstance().getDriver());
            if(connection == null)
                connection = DriverManager.getConnection(ConnectionProperties.getInstance().getUrl(),
                        ConnectionProperties.getInstance().getUser(), ConnectionProperties.getInstance().getPass());
        } catch (ClassNotFoundException | SQLException e) {
            log.error("cannot get connection", e);
        }
        return connection;
    }

    public void remove(int id) {
        try {
            log.info("file with id {} will be deleted", id);
            checkConnection("phoneid before delete");
            statement = connection.prepareStatement("DELETE FROM FILES WHERE file_id = ?;");
            statement.setInt(1, id);
            statement.executeQuery();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            closeStatement();
        }
    }

    public void update(AttachmentFile file) throws Exception {
        try {
            log.info("file with id {}, name {}, comment{} will be updated", file.getFileId(), file.getFileUserName(), file.getComment());
            if (connection == null) {
                getConnection();
            }
            statement = connection.prepareStatement("UPDATE FILES SET comment=?, file_name=? WHERE file_id=?");
            statement.setString(1, file.getComment());
            statement.setString(2,file.getFileUserName());
            statement.setInt(3, file.getFileId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            log.error(e);
        }
        finally {
            closeStatement();
        }
    }

    @Override
    public void insert(AttachmentFile file) throws Exception {
        try {
            log.info("file with id {}, name {}, comment {}, path {} will be insert", file.getFileId(), file.getFileUserName(), file.getComment(), file.getFileName());
            if (connection == null) {
                getConnection();
            }
            statement = connection.prepareStatement("INSERT INTO FILES (comment, file_location, file_name, person_id, date_of_upload) VALUES(?, ?, ?, ?, ?)");
            statement.setString(1, file.getComment());
            statement.setString(2,file.getFileName());
            statement.setString(3,file.getFileUserName());
            statement.setInt(4,file.getPersonId());
            statement.setTimestamp(5, new Timestamp(file.getDateOfUpload().getMillis()));
            statement.executeUpdate();
        }
        catch (SQLException e) {
            log.error(e);
        }
        finally {
            closeStatement();
        }
    }

    public List<AttachmentFile> getFilesOfPersonById(int personId) {
        log.info("getting files of person with id{}", personId);
        List<AttachmentFile> files = new ArrayList<AttachmentFile>();
        try {
            checkConnection("files get");
            statement = connection.prepareStatement("SELECT * FROM FILES WHERE person_id = ?;");
            ResultSet resultSetWithFiles = null;
            statement.setInt(1, personId);
            resultSetWithFiles = statement.executeQuery();
            while (resultSetWithFiles.next()) {
                files.add(getFileParameters(resultSetWithFiles));
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            closeStatement();
        }
        log.info("{} file was returned", files.size());
        return files;
    }

    public void closeConnection(){
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            log.error(e);
        }
    }

    private AttachmentFile getFileParameters(ResultSet resultSetWithFiles) throws SQLException {
        AttachmentFile file = new AttachmentFile();
        Integer bufInt = null;
        String bufString = null;
        bufInt = resultSetWithFiles.getInt("file_id");
        if (!resultSetWithFiles.wasNull()) {
            file.setFileId(bufInt);
        }
        bufInt = resultSetWithFiles.getInt("person_id");
        if (!resultSetWithFiles.wasNull()) {
            file.setPersonId(bufInt);
        }
        bufString = resultSetWithFiles.getString("comment");
        if (!resultSetWithFiles.wasNull()) {
            file.setComment(bufString);
        }
        bufString = resultSetWithFiles.getString("file_location");
        if (!resultSetWithFiles.wasNull()) {
            file.setFileName(bufString);
        }
        bufString = resultSetWithFiles.getString("file_name");
        if (!resultSetWithFiles.wasNull()) {
            file.setFileUserName(bufString);
        }
        return file;
    }



}
