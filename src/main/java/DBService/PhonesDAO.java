package DBService;

import Contact.Phone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonkw on 03.03.2015.
 */
public class PhonesDAO extends TemplateDAO {
    public PhonesDAO(Connection connection) {
        this.connection = connection;
    }

    Logger log = LogManager.getLogger(PhonesDAO.class);

    public void insert(List<Phone> phones, int idOfContact) {
        log.info("inserting {} phones of contact with id = {}", phones.size(), idOfContact);
        try {
            checkConnection("before insert");
            statement = connection.prepareStatement("INSERT INTO PHONES (country_code, operator_code, phone, type, comment, person_id) VALUES (?, ?, ?, ?, ?, ?);");
            for (Phone phone : phones) {
                phone.setPersonId(idOfContact);
                setStatementForPhoneInsert(phone);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            closeStatement();
        }
    }

    public void removePhone(int phoneId) {
        try {
            checkConnection("phoneid before delete");
            statement = connection.prepareStatement("DELETE FROM PHONES WHERE phone_id = ?;");
            statement.setInt(1, phoneId);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            closeStatement();
        }
    }

    public void updatePhone(Phone phone) {
        log.info("update phone with id {}, country {}, operator {}, phone {}, comment {}, type {}", phone.getPhoneId(),
                phone.getCountryCode(), phone.getOperatorCode(), phone.getPhone(), phone.getComment(), phone.getType());
        try {
            checkConnection("phoneid before update");
            statement = connection.prepareStatement("UPDATE PHONES SET country_code=?, operator_code=?, phone=?, type=?, comment=? WHERE phone_id = ?;");
            statement.setInt(1, phone.getCountryCode());
            statement.setInt(2, phone.getOperatorCode());
            statement.setInt(3, phone.getPhone());
            statement.setString(4, phone.getType());
            statement.setString(5, phone.getComment());
            statement.setInt(6, phone.getPhoneId());
            System.out.println(phone.getPhoneId());
            System.out.println(phone.getCountryCode());
            System.out.println(phone.getPhone());System.out.println(phone.getType());
            System.out.println(phone.getComment());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            closeStatement();
        }
    }

    public List<Phone> getPhonesOfPersonById(int personId) {
        log.info("get phones of person with id = {}", personId);

        List<Phone> phones = new ArrayList<Phone>();
        try {
            checkConnection("phones get");
            statement = connection.prepareStatement("SELECT * FROM PHONES WHERE person_id = ?;");
            ResultSet resultSetWithPhones;
            statement.setInt(1, personId);
            resultSetWithPhones = statement.executeQuery();
            while (resultSetWithPhones.next()) {
                phones.add(getPhonesParameters(resultSetWithPhones));
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            closeStatement();
        }
        return phones;
    }

    private void setStatementForPhoneInsert(Phone phone) throws SQLException {
        statement.setInt(1, phone.getCountryCode());
        statement.setInt(2, phone.getOperatorCode());
        statement.setInt(3, phone.getPhone());
        statement.setString(4, phone.getType());
        statement.setString(5, phone.getComment());
        statement.setInt(6, phone.getPersonId());
    }

    private Phone getPhonesParameters(ResultSet resultSetWithPhones) throws SQLException {
        Phone phone = new Phone();
        Integer bufInt;
        String bufString;
        bufInt = resultSetWithPhones.getInt("phone_id");
        if (!resultSetWithPhones.wasNull()) {
            phone.setPhoneId(bufInt);
        }
        bufInt = resultSetWithPhones.getInt("country_code");
        if (!resultSetWithPhones.wasNull()) {
            phone.setCountryCode(bufInt);
        }
        bufInt = resultSetWithPhones.getInt("operator_code");
        if (!resultSetWithPhones.wasNull()) {
            phone.setOperatorCode(bufInt);
        }
        bufInt = resultSetWithPhones.getInt("phone");
        if (!resultSetWithPhones.wasNull()) {
            phone.setPhone(bufInt);
        }
        bufInt = resultSetWithPhones.getInt("person_id");
        if (!resultSetWithPhones.wasNull()) {
            phone.setPersonId(bufInt);
        }
        bufString = resultSetWithPhones.getString("type");
        if (!resultSetWithPhones.wasNull()) {
            phone.setType(bufString);
        }
        bufString = resultSetWithPhones.getString("comment");
        if (!resultSetWithPhones.wasNull()) {
            phone.setComment(bufString);
        }
        return phone;
    }
}
