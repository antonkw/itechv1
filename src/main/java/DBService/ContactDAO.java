package DBService;

import Contact.*;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by antonkw on 17.02.2015.
 */
public interface ContactDAO {
    public int insert(Contact contact) throws Exception;

    public ArrayList<Contact> getSomeFullContacts(int startPosition, int count) throws Exception;
    public ArrayList<Contact> getSomeContactsForMainPage(int startPosition, int count) throws Exception;
    public void update(ContactForUpdate contact) throws Exception;
    public int getCount() throws Exception;
}
