package DBService;

import Contact.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonkw on 17.02.2015.
 */
public class JDBCContactsDAO extends TemplateDAO implements ContactDAO {

    Logger log = LogManager.getLogger(JDBCContactsDAO.class);

    public void startTransaction() {
        try {
            checkConnection("start transaction");
            Statement statementForStartTranscation = connection.createStatement();
            statementForStartTranscation.executeUpdate("START TRANSACTION;");
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void rollbackTransaction() {
        try {
            checkConnection("start transaction");
            Statement statementForStartTranscation = connection.createStatement();
            statementForStartTranscation.executeUpdate("ROLLBACK ;");
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void commitTransaction() {
        try {
            checkConnection("commit");
            Statement statementForCommitTranscation = connection.createStatement();
            statementForCommitTranscation.executeUpdate("COMMIT;");
        } catch (SQLException e) {
            rollbackTransaction();
            log.error(e.getMessage(),e);
        }
        finally {
            closeConnection();
        }
    }

    public Connection getConnection() {
        try {
            Class.forName(ConnectionProperties.getInstance().getDriver());
            if (connection == null) {
                connection = DriverManager.getConnection(ConnectionProperties.getInstance().getUrl(),
                        ConnectionProperties.getInstance().getUser(), ConnectionProperties.getInstance().getPass());
                Statement statementForStartTranscation = connection.createStatement();
                statementForStartTranscation.executeUpdate("SET autocommit = 0;");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return connection;
    }

    @Override
    public int insert(Contact contact) throws ParseException {
        log.info("contact with name {}, last name {} and mail {} wil be insert", contact.getName(), contact.getLastName(), contact.getEmail());
        int idOfInsertContact = 0;
        try {
            checkConnection("insert contact");
            if (connection == null) {
                getConnection();
            }
            setStatementForContactInsert(contact);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating contact failed, no rows affected.");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    idOfInsertContact = generatedKeys.getInt(1);
                    contact.setPersonId(idOfInsertContact);
                } else {
                    throw new SQLException("Creating contact failed, no ID obtained.");
                }
            }
            if (contact.getPhones() != null & contact.getPhones().size() != 0) {
                PhonesDAO phonesDAO = new PhonesDAO(connection);
                phonesDAO.insert(contact.getPhones(), contact.getPersonId());
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
        }
        return idOfInsertContact;
    }

    @Override
    public ArrayList<Contact> getSomeContactsForMainPage(int startPosition, int count) {
        log.info("{} contacts from the position {} will be get from DB", count, startPosition);
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        try {
            getConnection();

            statement = connection.prepareStatement("SELECT * FROM CONTACTS LIMIT ?, ?;");
            statement.setInt(1, startPosition);
            statement.setInt(2, count);
            ResultSet resultSet = statement.executeQuery();
            Contact contact = null;
            while (resultSet.next()) {
                contacts.add(setContactsParameters(resultSet, true));
            }
            resultSet.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }
        return contacts;
    }

    public ArrayList<Contact> getSomeContactsForRequest(int startPosition, int count, SearchRequest searchRequest) {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        try {
            getConnection();
            StringBuffer sql = new StringBuffer("SELECT * FROM CONTACTS WHERE ");
            boolean isFirst = true;
            if (searchRequest.getName() != null) {
                sql.append("name = ?");
                isFirst = false;
            }
            if (searchRequest.getLastName() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("last_name = ?");
            }
            if (searchRequest.getPatronymic() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("patronymic = ?");
            }
            if (searchRequest.getDateTime() != null) {

                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                if (searchRequest.isLater()) {
                    sql.append("date_of_birth <= ?");
                } else {
                    sql.append("date_of_birth >= ?");
                }

            }
            if (searchRequest.getCountryOfCitizenship() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("country_of_citizenship = ?");
            }
            if (searchRequest.getSex() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("sex = ?");
            }
            if (searchRequest.getMaritalStatus() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("marital_status = ?");
            }
            if (searchRequest.getCountry() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("country = ?");
            }
            if (searchRequest.getWork() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("work = ?");
            }
            if (searchRequest.getCity() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("city = ?");
            }

            sql.append(" LIMIT ?, ?;");
            int i = 0;
            statement = connection.prepareStatement(sql.toString());
            if (searchRequest.getName() != null) {
                ++i;
                statement.setString(i, searchRequest.getName());
            }
            if (searchRequest.getLastName() != null) {
                ++i;
                statement.setString(i, searchRequest.getLastName());
            }
            if (searchRequest.getPatronymic() != null) {
                ++i;
                statement.setString(i, searchRequest.getPatronymic());
            }
            if (searchRequest.getDateTime() != null) {
                ++i;
                statement.setDate(i, new Date(searchRequest.getDateTime().toDate().getTime()));
            }
            if (searchRequest.getCountryOfCitizenship() != null) {
                ++i;
                statement.setString(i, searchRequest.getCountryOfCitizenship());
            }
            if (searchRequest.getSex() != null) {
                ++i;
                statement.setString(i, searchRequest.getSex());
            }
            if (searchRequest.getMaritalStatus() != null) {
                ++i;
                statement.setString(i, searchRequest.getMaritalStatus());
            }
            if (searchRequest.getCountry() != null) {
                ++i;
                statement.setString(i, searchRequest.getCountry());
            }
            if (searchRequest.getWork() != null) {
                ++i;
                statement.setString(i, searchRequest.getWork());
            }
            if (searchRequest.getCity() != null) {
                ++i;
                statement.setString(i, searchRequest.getCity());
            }
            ++i;
            statement.setInt(i, startPosition);
            ++i;
            statement.setInt(i, count);
            ResultSet resultSet = statement.executeQuery();
            Contact contact = null;
            while (resultSet.next()) {
                contacts.add(setContactsParameters(resultSet, true));
            }
            System.out.println(contacts.size());
            resultSet.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }

        return contacts;
    }

    public Integer getCountOfRequestResult(SearchRequest searchRequest) {
        Integer count = 0;

        try {
            getConnection();
            StringBuffer sql = new StringBuffer("SELECT COUNT(*) FROM CONTACTS WHERE ");
            boolean isFirst = true;
            if (searchRequest.getName() != null) {
                sql.append("name = ?");
                isFirst = false;
            }
            if (searchRequest.getLastName() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("last_name = ?");
            }
            if (searchRequest.getPatronymic() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("patronymic = ?");
            }
            if (searchRequest.getDateTime() != null) {

                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                if (searchRequest.isLater()) {
                    sql.append("date_of_birth <= ?");
                } else {
                    sql.append("date_of_birth >= ?");
                }

            }
            if (searchRequest.getCountryOfCitizenship() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("country_of_citizenship = ?");
            }
            if (searchRequest.getSex() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("sex = ?");
            }
            if (searchRequest.getMaritalStatus() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("marital_status = ?");
            }
            if (searchRequest.getCountry() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("country = ?");
            }
            if (searchRequest.getWork() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("work = ?");
            }
            if (searchRequest.getCity() != null) {
                if (!isFirst) {
                    sql.append(" AND ");
                } else {
                    isFirst = false;
                }
                sql.append("city = ?");
            }

            log.info("try to get count of requesr resul with query \"{}\"", sql.toString());


            int i = 0;
            statement = connection.prepareStatement(sql.toString());
            if (searchRequest.getName() != null) {
                ++i;
                statement.setString(i, searchRequest.getName());
            }
            if (searchRequest.getLastName() != null) {
                ++i;
                statement.setString(i, searchRequest.getLastName());
            }
            if (searchRequest.getPatronymic() != null) {
                ++i;
                statement.setString(i, searchRequest.getPatronymic());
            }
            if (searchRequest.getDateTime() != null) {
                ++i;
                statement.setDate(i, new Date(searchRequest.getDateTime().toDate().getTime()));
            }
            if (searchRequest.getCountryOfCitizenship() != null) {
                ++i;
                statement.setString(i, searchRequest.getCountryOfCitizenship());
            }
            if (searchRequest.getSex() != null) {
                ++i;
                statement.setString(i, searchRequest.getSex());
            }
            if (searchRequest.getMaritalStatus() != null) {
                ++i;
                statement.setString(i, searchRequest.getMaritalStatus());
            }
            if (searchRequest.getCountry() != null) {
                ++i;
                statement.setString(i, searchRequest.getCountry());
            }
            if (searchRequest.getWork() != null) {
                ++i;
                statement.setString(i, searchRequest.getWork());
            }
            if (searchRequest.getCity() != null) {
                ++i;
                statement.setString(i, searchRequest.getCity());
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                count = resultSet.getInt("COUNT(*)");
                if (resultSet.wasNull()) {
                    count = 0;
                }
            }

            resultSet.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }
        log.info("{} contacts was found by request", count);
        return count;
    }

    public List<Contact> getContactsWithMail(List <Integer> persons) {
        log.info("{} contacts only with e-mail will be select from DB", persons.size());
        List<Contact> contacts = new ArrayList<Contact>();
        try {
            getConnection();
            boolean isFirst = true;
            StringBuilder sql = new StringBuilder("SELECT name, e_mail FROM CONTACTS WHERE") ;
            for (Integer i: persons) {
                if (!isFirst) {
                    sql.append(" OR");
                } else {
                    isFirst = false;
                }
                sql.append(" person_id = ?");
            }
            int index = 0;
            sql.append(";");
            System.out.println(sql.toString());
            statement = connection.prepareStatement(sql.toString());
            for (Integer i: persons) {
                ++index;
                statement.setInt(index, i);
            }
            ResultSet resultSet = statement.executeQuery();
            Contact contact = null;
            while (resultSet.next()) {
                contact = new Contact();
                String bufString = resultSet.getString("name");
                if (!resultSet.wasNull()) {
                    contact.setName(bufString);
                }
                bufString = resultSet.getString("e_mail");
                if (!resultSet.wasNull()) {
                    contact.setEmail(bufString);
                }
                contacts.add(contact);
            }
            resultSet.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }
        log.info("{} contacts returning with e-mail", contacts.size());
        return contacts;
    }


    public ArrayList<Contact> getContactsWithBirthDays() {
        log.info("start checking birthdays in database");
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        try {
            getConnection();
            statement = connection.prepareStatement("SELECT name, e_mail FROM CONTACTS WHERE MONTH(date_of_birth) = ? AND DAYOFMONTH(date_of_birth) = ?;");

            DateTime todayDate = new DateTime(DateTime.now());

            statement.setInt(1, todayDate.getMonthOfYear());
            log.info("month of current date: {}", todayDate.getMonthOfYear());
            statement.setInt(2, todayDate.getDayOfMonth());
            log.info("dayof month of current dat: {}", todayDate.getDayOfMonth());

            ResultSet resultSet = statement.executeQuery();
            Contact contact = null;

            while (resultSet.next()) {
                contact = new Contact();
                String bufString = resultSet.getString("name");
                if (!resultSet.wasNull()) {
                    contact.setName(bufString);
                }
                bufString = resultSet.getString("e_mail");
                if (!resultSet.wasNull()) {
                    contact.setEmail(bufString);
                }

                contacts.add(contact);
            }
            resultSet.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }
        log.info("count of contacts with birthday: {}", contacts.size());
        return contacts;

    }



    @Override
    public ArrayList<Contact> getSomeFullContacts(int startPosition, int count) {
        log.info("start selecting {} full contacts from database from id {}", count, startPosition);
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        try {
            getConnection();
            startTransaction();
            statement = connection.prepareStatement("SELECT * FROM CONTACTS LIMIT ?, ?;");
            statement.setInt(1, startPosition);
            statement.setInt(2, count);
            ResultSet resultSet = statement.executeQuery();
            Contact contact = null;
            PhonesDAO phonesDAO = new PhonesDAO(connection);
            while (resultSet.next()) {
                contact = setContactsParameters(resultSet, false);
                contact.setPhones(phonesDAO.getPhonesOfPersonById(contact.getPersonId()));
                contacts.add(contact);
            }
            resultSet.close();
            commitTransaction();
        } catch (SQLException e) {
            rollbackTransaction();
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }
        return contacts;
    }

    public Contact getFullContact(int idOfContact) throws SQLException{
        log.info("get full contact with id {}", idOfContact);
        Contact contact = new Contact();
        try {
            getConnection();
            startTransaction();
            statement = connection.prepareStatement("SELECT * FROM CONTACTS WHERE person_id = ?;");
            statement.setInt(1, idOfContact);

            ResultSet resultSet = statement.executeQuery();

            PhonesDAO phonesDAO = new PhonesDAO(connection);
            JDBCFilesDAO jdbcFilesDAO = new JDBCFilesDAO(connection);
            if (resultSet.next()) {
                contact = setContactsParameters(resultSet, false);
                contact.setPhones(phonesDAO.getPhonesOfPersonById(contact.getPersonId()));
                contact.setFiles(jdbcFilesDAO.getFilesOfPersonById(contact.getPersonId()));
            }
            else {
                throw new SQLException("no contact found in DB");
            }
            commitTransaction();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            rollbackTransaction();
            throw e;
        } finally {
            closeStatement();
            closeConnection();
        }
        return contact;
    }

    public void removeContacts(List <Integer> contactsToRemove) {
        log.info("remove {} contacts", contactsToRemove.size());
        try {
            getConnection();
            startTransaction();
            statement = connection.prepareStatement("DELETE FROM CONTACTS WHERE person_id = ?;");
            for (Integer id : contactsToRemove) {
                statement.setInt(1, id);
                statement.executeUpdate();
            }
            commitTransaction();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            rollbackTransaction();
        } finally {
            closeStatement();
            closeConnection();
        }
    }




    @Override
    public void update(ContactForUpdate contact) throws Exception {
        try {
            if (connection == null) {
                getConnection();
            }
            startTransaction();
            setStatementForContactUpdate(contact);
            statement.executeUpdate();
            PhonesDAO phonesDAO = null;
            JDBCFilesDAO jdbcFilesDAO = null;

            if (contact.getPhones() != null & contact.getPhones().size() != 0) {
                phonesDAO = new PhonesDAO(connection);
                phonesDAO.insert(contact.getPhones(), contact.getPersonId());
            }
            if (contact.getPhonesToUpdate() != null & contact.getPhonesToUpdate().size() != 0) {
                if (phonesDAO == null) {
                    phonesDAO = new PhonesDAO(connection);
                }
                for (Phone phone: contact.getPhonesToUpdate()) {
                    phonesDAO.updatePhone(phone);
                }
            }
            if (contact.getPhonesToDelete() != null & contact.getPhonesToDelete().size() != 0) {
                if (phonesDAO == null) {
                    phonesDAO = new PhonesDAO(connection);
                }
                for (Integer phone: contact.getPhonesToDelete()) {
                    phonesDAO.removePhone(phone);
                }
            }
            if (contact.getFilesToDelete() != null & contact.getFilesToDelete().size() != 0) {
                jdbcFilesDAO = new JDBCFilesDAO(connection);
                for (Integer file: contact.getFilesToDelete()) {
                    jdbcFilesDAO.remove(file);
                }
            }
            if (contact.getFilesToUpdate() != null & contact.getFilesToUpdate().size() != 0) {
                if (jdbcFilesDAO == null) {
                    jdbcFilesDAO = new JDBCFilesDAO(connection);
                }
                for (AttachmentFile file: contact.getFilesToUpdate()) {
                    jdbcFilesDAO.update(file);
                }

            }
            if (contact.getFiles() != null & contact.getFiles().size() != 0) {
                if (jdbcFilesDAO == null) {
                    jdbcFilesDAO = new JDBCFilesDAO(connection);
                }
                for (AttachmentFile file: contact.getFiles()) {
                    jdbcFilesDAO.insert(file);
                }
            }
            commitTransaction();
        } catch (SQLException e) {
            rollbackTransaction();
            log.error(e.getMessage(), e);
        } finally {
            closeStatement();
            closeConnection();
        }
    }

    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getCount() {
        log.info("getting count of all contacts");
        Statement statement = null;
        int count = 0;
        try {
            getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM CONTACTS;");
            if (resultSet.next()) {
                count = resultSet.getInt("COUNT(*)");
                if (resultSet.wasNull()) {
                    count = 0;
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            rollbackTransaction();
        }
        finally {
            closeStatement();
            closeConnection();
        }
        log.info("{} - count of contacts", count);
        return count;
    }

    private void setStatementForPhoneInsert(Phone phone) throws SQLException {

        statement.setInt(1, phone.getCountryCode());
        statement.setInt(2, phone.getOperatorCode());
        statement.setInt(3, phone.getPhone());
        statement.setString(4, phone.getType());
        statement.setString(5, phone.getComment());
        statement.setInt(6, phone.getPersonId());
    }

    private void setStatementForContactInsert(Contact contact) throws SQLException, ParseException {
        StringBuilder fieldsNamePart = new StringBuilder("INSERT INTO CONTACTS (NAME, LAST_NAME, E_MAIL");
        StringBuilder valuesPart = new StringBuilder(") VALUES (?, ?, ?");
        int currentIndex = 4;
        int patronymicIndex = 0;
        int dateIndex = 0;
        int sexIndex = 0;
        int citizenIndex = 0;
        int maritalIndex = 0;
        int workIndex = 0;
        int countryIndex = 0;
        int locationIndex = 0;
        int cityIndex = 0;
        int zipIndex = 0;
        if (contact.getPatronymic() != null) {
            fieldsNamePart.append(", PATRONYMIC");
            valuesPart.append(", ?");
            patronymicIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getDate() != null) {
            fieldsNamePart.append(", DATE_OF_BIRTH");
            valuesPart.append(", ?");
            dateIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getSex() != null) {
            fieldsNamePart.append(", SEX");
            valuesPart.append(", ?");
            sexIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getCountryOfCitizenship() != null) {
            fieldsNamePart.append(", COUNTRY_OF_CITIZENSHIP");
            valuesPart.append(", ?");
            citizenIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getMaritalStatus() != null) {
            fieldsNamePart.append(", MARITAL_STATUS");
            valuesPart.append(", ?");
            maritalIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getWork() != null) {
            fieldsNamePart.append(", WORK");
            valuesPart.append(", ?");
            workIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getCountry() != null) {
            fieldsNamePart.append(", COUNTRY");
            valuesPart.append(", ?");
            countryIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getCity() != null) {
            fieldsNamePart.append(", CITY");
            valuesPart.append(", ?");
            cityIndex = currentIndex;
            ++currentIndex;

        }
        if (contact.getLocation() != null) {
            fieldsNamePart.append(", LOCATION");
            valuesPart.append(", ?");
            locationIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getZipcode() != null) {
            fieldsNamePart.append(", ZIP_CODE");
            valuesPart.append(", ?");
            zipIndex = currentIndex;
        }

        valuesPart.append(");");
        fieldsNamePart.append(valuesPart);
        log.info("insert contacts with statement {}", fieldsNamePart.toString());
        statement = connection.prepareStatement(fieldsNamePart.toString(), Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, contact.getName());
        statement.setString(2, contact.getLastName());
        statement.setString(3, contact.getEmail());
        if (patronymicIndex != 0) {
            statement.setString(patronymicIndex, contact.getPatronymic());
        }
        if (dateIndex != 0) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(format.parse(contact.getDate()).getTime());
            statement.setDate(dateIndex, date);
        }
        if (sexIndex != 0) {
            statement.setString(sexIndex, contact.getSex());
        }
        if (citizenIndex != 0) {
            statement.setString(citizenIndex, contact.getCountryOfCitizenship());
        }
        if (maritalIndex != 0) {
            statement.setString(maritalIndex, contact.getMaritalStatus());
        }
        if (workIndex != 0) {
            statement.setString(workIndex, contact.getWork());
        }
        if (countryIndex != 0) {
            statement.setString(countryIndex, contact.getCountry());
        }
        if (locationIndex != 0) {
            statement.setString(locationIndex, contact.getLocation());
        }
        if (cityIndex != 0) {
            statement.setString(cityIndex, contact.getCity());
        }
        if (zipIndex != 0) {
            statement.setInt(zipIndex, contact.getZipcode());
        }
    }

    private void setStatementForContactUpdate(Contact contact) throws SQLException {

        StringBuilder sql = new StringBuilder("UPDATE CONTACTS SET NAME = ?, LAST_NAME = ?, E_MAIL = ?");
        int currentIndex = 4;
        int patronymicIndex = 0;
        int dateIndex = 0;
        int sexIndex = 0;
        int citizenIndex = 0;
        int maritalIndex = 0;
        int workIndex = 0;
        int countryIndex = 0;
        int locationIndex = 0;
        int cityIndex = 0;
        int zipIndex = 0;
        int avatarIndex = 0;
        int idIndex = 0;
        if (contact.getPatronymic() != null) {
            sql.append(", PATRONYMIC=?");
            patronymicIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getDate() != null) {
            sql.append(", DATE_OF_BIRTH=?");
            dateIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getSex() != null) {
            sql.append(", SEX=?");
            sexIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getCountryOfCitizenship() != null) {
            sql.append(", COUNTRY_OF_CITIZENSHIP=?");
            citizenIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getMaritalStatus() != null) {
            sql.append(", MARITAL_STATUS=?");
            maritalIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getWork() != null) {
            sql.append(", WORK=?");
            workIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getCountry() != null) {
            sql.append(", COUNTRY=?");
            countryIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getCity() != null) {
            sql.append(", CITY=?");
            cityIndex = currentIndex;
            ++currentIndex;

        }
        if (contact.getLocation() != null) {
            sql.append(", LOCATION=?");
            locationIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getZipcode() != null) {
            sql.append(", ZIP_CODE=?");
            zipIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getAvatar()!= null) {
            sql.append(", AVATAR=?");
            avatarIndex = currentIndex;
            ++currentIndex;
        }
        if (contact.getPersonId() != null) {
            sql.append(" WHERE PERSON_ID=?;");
            idIndex = currentIndex;
        } else {
            throw new SQLException("no id in insert contact");
        }

        log.info ("updare contact with id {} with query {}", contact.getPersonId(), sql.toString());

        statement = connection.prepareStatement(sql.toString());
        statement.setString(1, contact.getName());
        statement.setString(2, contact.getLastName());
        statement.setString(3, contact.getEmail());
        if (patronymicIndex != 0) {
            statement.setString(patronymicIndex, contact.getPatronymic());
        }
        if (dateIndex != 0) {
            statement.setString(dateIndex, contact.getDate());
        }
        if (sexIndex != 0) {
            statement.setString(sexIndex, contact.getSex());
        }
        if (citizenIndex != 0) {
            statement.setString(citizenIndex, contact.getCountryOfCitizenship());
        }
        if (maritalIndex != 0) {
            statement.setString(maritalIndex, contact.getMaritalStatus());
        }
        if (workIndex != 0) {
            statement.setString(workIndex, contact.getWork());
        }
        if (countryIndex != 0) {
            statement.setString(countryIndex, contact.getCountry());
        }
        if (locationIndex != 0) {
            statement.setString(locationIndex, contact.getLocation());
        }
        if (cityIndex != 0) {
            statement.setString(cityIndex, contact.getCity());
        }
        if (zipIndex != 0) {
            statement.setInt(zipIndex, contact.getZipcode());
        }
        if (avatarIndex != 0) {
            statement.setString(avatarIndex, contact.getAvatar());
        }
        if (idIndex != 0) {
            statement.setInt(idIndex, contact.getPersonId());
        }

    }



    private Contact setContactsParameters(ResultSet resultSet, boolean isShortInfo) throws SQLException {
        Contact contact = new Contact();

        contact.setPersonId(Integer.parseInt(resultSet.getString("person_id")));
        log.info("set contact (id = {}) parameters", contact.getPersonId());
        String bufString;
        bufString = resultSet.getString("name");
        if (!resultSet.wasNull()) {
            contact.setName(bufString);
        }
        bufString = resultSet.getString("last_name");
        if (!resultSet.wasNull()) {
            contact.setLastName(bufString);
        }
        bufString = resultSet.getString("patronymic");
        if (!resultSet.wasNull()) {
            contact.setPatronymic(bufString);
        }
        bufString = resultSet.getString("date_of_birth");
        if (!resultSet.wasNull()) {
            contact.setDate(bufString);
        }
        bufString = resultSet.getString("e_mail");
        if (!resultSet.wasNull()) {
            contact.setEmail(bufString);
        }
        bufString = resultSet.getString("work");
        if (!resultSet.wasNull()) {
            contact.setWork(bufString);
        }
        bufString = resultSet.getString("country");
        if (!resultSet.wasNull()) {
            contact.setCountry(bufString);
        }
        bufString = resultSet.getString("city");
        if (!resultSet.wasNull()) {
            contact.setCity(bufString);
        }
        bufString = resultSet.getString("location");
        if (!resultSet.wasNull()) {
            contact.setLocation(bufString);
        }

        if (!isShortInfo) {
            bufString = resultSet.getString("zip_code");
            if (!resultSet.wasNull()) {
                contact.setZipcode(Integer.parseInt(bufString));
            }

            bufString = resultSet.getString("country_of_citizenship");
            if (!resultSet.wasNull()) {
                contact.setCountryOfCitizenship(bufString);
            }
            bufString = resultSet.getString("sex");
            if (!resultSet.wasNull()) {
                contact.setSex(bufString);
            }
            bufString = resultSet.getString("marital_status");
            if (!resultSet.wasNull()) {
                contact.setMaritalStatus(bufString);
            }
            bufString = resultSet.getString("avatar");
            if (!resultSet.wasNull()) {
                contact.setAvatar(bufString);
            }
        }
        return contact;
    }
}
