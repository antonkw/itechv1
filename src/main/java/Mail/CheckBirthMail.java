package Mail;

import Contact.Contact;
import DBService.JDBCContactsDAO;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonkw on 12.03.2015.
 */
public class CheckBirthMail {
    public void check() throws Exception {
        JDBCContactsDAO jdbcContactsDAO = new JDBCContactsDAO();
        List<Contact> contacts = jdbcContactsDAO.getContactsWithBirthDays();

        List<MailObject> mailObjects = new ArrayList<>();
        MailService mailService1 = new MailService();
        List <String> mails = new ArrayList<>();
        STGroup birthday = new STGroupFile("birth.stg");
        ST birthTemplate = birthday.getInstanceOf("t2");
        for (Contact contact: contacts) {
            StringBuilder builder = new StringBuilder(contact.getName());
            builder.append("(");
            builder.append(contact.getEmail());
            builder.append(")");
            mails.add(builder.toString());
        }
        if (contacts.size() != 0) {
            birthTemplate.add("arg1", AdminProperties.getInstance().getName());
            birthTemplate.add("contacts", mails);
            MailObject mailObject = new MailObject();
            mailObject.setText(birthTemplate.render());
            mailObject.getAdresses().add(AdminProperties.getInstance().getMail());
            mailObject.setTheme("User`s birthdays");
            mailObjects.add(mailObject);
            mailService1.SendMail(mailObject);
            try {
                mailService1.SendMails(mailObjects);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }


    }
}
