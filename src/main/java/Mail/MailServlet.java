package Mail;

import Contact.Contact;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by antonkw on 12.03.2015.
 */
public class MailServlet extends HttpServlet {
    Logger log = LogManager.getLogger(MailServlet.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MailObject mailObject = null;
        String templateName = null;
        String message = null;
        String theme = null;
        STGroup spam = null;

        ST spamTemlate = null;

        List<Contact> contacts = (List<Contact>) request.getSession(false).getAttribute("mails");
        if (!StringUtils.isEmpty(request.getParameter("templateName"))) {
            templateName = request.getParameter("templateName");
        } else {
            ServletException e = new ServletException("template name empty!");
            log.error(e);
            throw(e);
        }
        if (!StringUtils.isEmpty(request.getParameter("theme"))) {
            theme = request.getParameter("theme");
        } else {
            ServletException e = new ServletException("theme empty!");
            log.error(e);
            throw(e);
        }

        if ("empty".equals(templateName)) {
            if (!StringUtils.isEmpty(request.getParameter("mail"))) {
                message = request.getParameter("mail");
            } else {
                ServletException e = new ServletException("mail field is empty!");
                log.error(e);
                throw(e);
            }
        } else if ("phones".equals(templateName)) {
            spam = new STGroupFile("spam.stg", '$', '$');
            spamTemlate = spam.getInstanceOf("spam1");
        } else if ("version".equals(templateName)) {
            spam = new STGroupFile("spam.stg", '$', '$');
            spamTemlate = spam.getInstanceOf("spam2");
        } else {
            ServletException e = new ServletException("empty theme");
            log.error(e);
            throw(e);
        }
        if (spamTemlate != null) {
            spamTemlate.add("admin", AdminProperties.getInstance().getName());
            spamTemlate.add("mail", AdminProperties.getInstance().getMail());
        }


        MailService service = new MailService();
        for (Contact contact: contacts) {
            mailObject = new MailObject();
            mailObject.setTheme(theme);
            mailObject.getAdresses().add(contact.getEmail());
            if ("empty".equals(templateName)) {
                mailObject.setText(message);
            } else if ("phones".equals(templateName)) {
                spamTemlate.remove("username");
                spamTemlate.add("username", contact.getName());
                mailObject.setText(spamTemlate.render());
            } else if ("version".equals(templateName)) {
                mailObject.setText(spamTemlate.render());
            }
            try {
                service.SendMail(mailObject);
            }  catch (Exception e) {
                log.error(e);
            }

        }
        request.getSession(false).removeAttribute("mails");
        response.sendRedirect("/index.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
