package Mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

/**
 * Created by antonkw on 12.03.2015.
 */
public class MailService {
    Logger log = LogManager.getLogger(MailService.class);

    private Session session = null;

    public void checkSession() throws Exception {

        if (session == null) {
            session = Session.getInstance(MailProperties.getInstance().getProp(),
                    new javax.mail.Authenticator() {
                        PasswordAuthentication passwordAuthentication = null;
                        protected PasswordAuthentication getPasswordAuthentication() {
                            try {
                                passwordAuthentication = new PasswordAuthentication(
                                        MailProperties.getInstance().getUser(), MailProperties.getInstance().getPass());
                            } catch (Exception e) {
                                log.error(e);
                            }
                            return passwordAuthentication;
                        }
                    });


        }

    }


    public void SendMail (MailObject mailObject) throws Exception {
        checkSession();
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(MailProperties.getInstance().getUser()));
        for (String address: mailObject.getAdresses()) {
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
        }

        message.setSubject(mailObject.getTheme());
        message.setText(mailObject.getText());

        Transport.send(message);

    }

    public void SendMails (List<MailObject> mailObjects) throws Exception {
        checkSession();

        for (MailObject mailObject: mailObjects) {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(MailProperties.getInstance().getUser()));
            for (String address : mailObject.getAdresses()) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
            }
            message.setSubject(mailObject.getTheme());
            message.setText(mailObject.getText());
            Transport.send(message);
        }


    }
}
