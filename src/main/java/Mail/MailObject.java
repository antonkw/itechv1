package Mail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonkw on 12.03.2015.
 */
public class MailObject {
    private String text;
    private List<String> adresses = new ArrayList<>();
    private String theme;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getAdresses() {
        return adresses;
    }

    public void setAdresses(List<String> adresses) {
        this.adresses = adresses;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}
