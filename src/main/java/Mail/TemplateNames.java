package Mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by antonkw on 13.03.2015.
 */
public class TemplateNames {
    private static TemplateNames instance;
    private Properties prop = null;


    private TemplateNames() {
        prop = new Properties();
        InputStream input = null;

        try {
            String propFileName = "templates_name.properties";
            input = getClass().getClassLoader().getResourceAsStream(propFileName);
            if(input == null){
                System.out.println("Sorry, unable to find " + propFileName);
                return;
            }

            prop.load(input);



        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static synchronized TemplateNames getInstance() {
        if (instance == null) {
            instance = new TemplateNames();
        }
        return instance;
    }

    public Properties getProp() { return prop; }
}
