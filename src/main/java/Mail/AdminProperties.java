package Mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by antonkw on 13.03.2015.
 */
public class AdminProperties {
    private static AdminProperties instance;
    private Properties prop = null;
    Logger log = LogManager.getLogger(AdminProperties.class);
    private String name;
    private String mail;

    private AdminProperties() {
        prop = new Properties();
        InputStream input = null;
        try {
            String propFileName = "admin_config.properties";
            input = getClass().getClassLoader().getResourceAsStream(propFileName);
            if(input == null){
                System.out.println("Sorry, unable to find " + propFileName);
                return;
            }
            prop.load(input);
            name = prop.getProperty("admin_name");
            mail = prop.getProperty("admin_mail");
        } catch (IOException ex) {
            log.error(ex);
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }

    }

    public static synchronized AdminProperties getInstance() {
        if (instance == null) {
            instance = new AdminProperties();
        }
        return instance;
    }


    public Properties getProp() { return prop; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
