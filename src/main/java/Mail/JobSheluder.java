package Mail;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by antonkw on 12.03.2015.
 */
@WebListener
public class JobSheluder implements ServletContextListener {
    Logger log = LogManager.getLogger(JobSheluder.class);

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        System.out.println("ServletContextListener destroyed");
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        try {
            // specify the job' s details..
            JobDetail job = JobBuilder.newJob(BirthJob.class)
                    .withIdentity("testJob")
                    .build();
            CronTriggerImpl trigger = new CronTriggerImpl("hello");
            trigger.setCronExpression("5 * * * * ? *"); /// every 5 seconds for test, 0 0 5 * * ? * for every day
            SchedulerFactory schFactory = new StdSchedulerFactory();
            Scheduler sch = schFactory.getScheduler();
            sch.start();
            sch.scheduleJob(job, trigger);

        } catch (Exception e) {
            log.error(e);
        }
    }




}