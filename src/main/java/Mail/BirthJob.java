package Mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.mail.MessagingException;
import javax.servlet.annotation.WebListener;


/**
 * Created by antonkw on 12.03.2015.
 */

@WebListener
public class BirthJob implements Job{
    Logger log = LogManager.getLogger(BirthJob.class);

    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
        log.info("checking for birthdays start");
        CheckBirthMail checkBirthMail = new CheckBirthMail();
        try {
            checkBirthMail.check();
        } catch (MessagingException e) {
            log.error("Cannot check birthdays", e );
        } catch (Exception e) {
            log.error(e);
        }
    }

}
