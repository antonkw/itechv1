package Mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by antonkw on 12.03.2015.
 */
public class MailProperties {
    Logger log = LogManager.getLogger(MailProperties.class);
    private static MailProperties instance;
    private Properties prop = null;
    private String user;
    private String pass;
    private MailProperties() throws Exception {
        prop = new Properties();
        InputStream input = null;
        try {
            String propFileName = "mail_config.properties";
            input = getClass().getClassLoader().getResourceAsStream(propFileName);
            if(input == null){
                Exception e = new Exception("unable to find prop. file");
                log.error(e);
                throw(e);
            }
            prop.load(input);
            user = prop.getProperty("user");
            pass = prop.getProperty("pass");
        } catch (IOException ex) {
            log.error(ex);
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }
    }

    public static synchronized MailProperties getInstance() throws Exception {
        if (instance == null) {
            instance = new MailProperties();
        }
        return instance;
    }


    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public Properties getProp() { return prop; }
}
