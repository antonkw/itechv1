<%@ page import="Contact.*" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="javax.imageio.ImageIO" %>
<%--
  Created by IntelliJ IDEA.
  User: antonkw
  Date: 16.02.2015
  Time: 21:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!
  boolean edit = false;
  Contact contact = null;


  %>

<%
  if ("edit".equals((String) request.getAttribute("context"))) {
    edit = true;
    contact = (Contact) request.getAttribute("contact");
  } else{
    edit = false;
    contact = null;
  }


%>

<form  enctype="multipart/form-data" accept-charset="UTF-8" class="contact_form" action="<% if (edit) out.print("edit"); else out.print("addContact");%>" method="post" name="contact_form">

  <table width="80%">


    <tr valign="TOP">
    <td width="30%" align="left">





  <div>
  <ul>
    <li id="first_pos">
      <h2><% out.print((String) request.getAttribute("context")); %></h2>
      <span class="required_notification">* Denotes Required Field</span>
    </li>
    <li>
      <label for="name">Name:</label>
      <input value="<% if (edit && contact.getName() != null) {
      out.print(contact.getName());}%>" class="text_input" type="text" name="name" required pattern="^[a-zA-Zа-яА-Я][a-zа-я]{1,20}$"/>
    </li>
    <li>
      <label for="lastName">Last Name:</label>
      <input value="<% if (edit && contact.getLastName() != null) {
      out.print(contact.getLastName());}%>" class="text_input" type="text" name="lastName" required pattern="^[a-zA-Zа-яА-Я][a-zа-я]{1,20}$"/>
    </li>
    <li>
      <label for="patronymic">Patronymic:</label>
      <input value="<% if (edit && contact.getPatronymic() != null) {
      out.print(contact.getPatronymic());}%>" class="text_input" type="text" name="patronymic" pattern="^[a-zA-Zа-яА-Я][a-zа-я]{1,20}$"/>
    </li>
    <li>
      <label for="dateOfBirth">Date of birth:</label>
      <span style="position: relative;"><input class="text_input" onfocusout="g_calendarObject.getSelectedDayFormatted()" type="text" value="<% if (edit && contact.getDate() != null) {
      out.print(contact.getDate());}%>" size="12" id="inputField" globalnumber="495"><div class="JsDatePickBox" globalcalnumber="495" style="z-index: 3; position: absolute; top: 18px; left: 0px; display: none;"><div class="boxLeftWall"><div class="leftTopCorner"></div><div class="leftWall" style="height: 231px;"></div><div class="leftBottomCorner"></div></div><div class="boxMain"><div class="boxMainInner"><div class="controlsBar" globalnumber="495"><div class="monthForwardButton" globalnumber="495"></div><div class="monthBackwardButton" globalnumber="495"></div><div class="yearForwardButton" globalnumber="495"></div><div class="yearBackwardButton" globalnumber="495"></div><div class="controlsBarText">March, 2015</div></div><div class="clearfix"></div><div class="tooltip"></div><div class="weekDaysRow"><div class="weekDay">Mon</div><div class="weekDay">Tue</div><div class="weekDay">Wed</div><div class="weekDay">Thu</div><div class="weekDay">Fri</div><div class="weekDay">Sat</div><div class="weekDay" style="margin-right: 0px;">Sun</div></div><div class="boxMainCellsContainer"><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">1</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">2</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">3</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">4</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">5</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">6</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">7</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">8</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">9</div><div globalnumber="495" istoday="1" class="dayNormalToday" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">10</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">11</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">12</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">13</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">14</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">15</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">16</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">17</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">18</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">19</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">20</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">21</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">22</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">23</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">24</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">25</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">26</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">27</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">28</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">29</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">30</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">31</div></div><div class="clearfix"></div></div></div><div class="boxRightWall"><div class="rightTopCorner"></div><div class="rightWall" style="height: 231px;"></div><div class="rightBottomCorner"></div></div><div class="clearfix"></div><div class="jsDatePickCloseButton" globalnumber="495"></div><div class="topWall"></div><div class="bottomWall"></div></div></span>
      <input value="<% if (edit && contact.getDate() != null) {
      out.print(contact.getDate());}%>"  type="hidden" id="dateOfBirth" name="dateOfBirth" />
    </li>

    <li>
      <label for="citizenship">Country of citizenship:</label>
      <select class="text-input" name="citizenship">
        <option value="UK" <%if (edit && "UK".equals(contact.getCountryOfCitizenship())) out.print("checked");%>>UK</option>
        <option value="Belarus" <%if (edit && "Belarus".equals(contact.getCountryOfCitizenship())) out.print("checked");%>>Belarus</option>
        <option value="Germany" <%if (edit && "Germany".equals(contact.getCountryOfCitizenship())) out.print("checked");%>>Germany</option>
        <option value="USA" <%if (edit && "USA".equals(contact.getCountryOfCitizenship())) out.print("checked");%>>USA</option>
        <option value="Norway" <%if (edit && "Norway".equals(contact.getCountryOfCitizenship())) out.print("checked");%>>Norway</option>
      </select>

    </li>
    <li>
      <label for="sex">Sex:</label>
      <input type="radio" name="sex" value="male" <% if (edit && "male".equals(contact.getSex())) {
      out.print("checked");} else if (!edit) {out.print("checked");}%>>Male
      <input type="radio" name="sex" value="female" <% if (edit && "female".equals(contact.getSex())) {
      out.print("checked");}%>>Female
    </li>
    <li>
      <label for="maritalStatus">Marital status:</label>
      <input type="radio" name="maritalStatus" value="unmarried" <% if (edit && "unmarried".equals(contact.getMaritalStatus())) {
      out.print("checked");} else if (!edit) {out.print("checked");}%>>Unmarried
      <input type="radio" name="maritalStatus" value="married" <% if (edit && "married".equals(contact.getMaritalStatus())) {
      out.print("checked");}%>>Married
      <input type="radio" name="maritalStatus" value="divorced" <% if (edit && "divorced".equals(contact.getMaritalStatus())) {
      out.print("checked");}%>>Divorced
      <input type="radio" name="maritalStatus" value="widowed" <% if (edit && "widowed".equals(contact.getMaritalStatus())) {
      out.print("checked");}%>>Widowed
    </li>
    <li>
      <label for="website">Website:</label>
      <input value="<% if (edit && contact.getSite() != null) {
      out.print(contact.getSite());}%>" class="text_input" type="url" name="website"  placeholder="http://kovalevsky.by" pattern="(http|https)://.+"/>
      <span class="form_hint">Proper format "http://kovalevsky.by"</span>
    </li>
    <li>
      <label for="email">E-mail:</label>
      <input value="<% if (edit && contact.getEmail() != null) {
      out.print(contact.getEmail());}%>" class="text_input" type="email" name="email" placeholder="antonkw.mail@gmail.com" required />
      <span class="form_hint">Proper format "antonkw.mail@gmail.com"</span>
    </li>
    <li>
      <label for="work">Current place of work:</label>
      <input value="<% if (edit && contact.getWork() != null) {
      out.print(contact.getWork());}%>" class="text_input" type="text" name="work" pattern="^[a-zA-Zа-яА-Я][a-zа-я\s]{1,20}$"/>
    </li>
    <li>
      <label>Adress:</label> <br>
      <ul id="adressList">
        <li>
          <label for="country">Country:</label>
          <select class="text-input" name="citizenship">
            <option value="UK" <%if (edit && "UK".equals(contact.getCountry())) out.print("checked");%>>UK</option>
            <option value="Belarus" <%if (edit && "Belarus".equals(contact.getCountry())) out.print("checked");%>>Belarus</option>
            <option value="Germany" <%if (edit && "Germany".equals(contact.getCountry())) out.print("checked");%>>Germany</option>
            <option value="USA" <%if (edit && "USA".equals(contact.getCountry())) out.print("checked");%>>USA</option>
            <option value="Norway" <%if (edit && "Norway".equals(contact.getCountry())) out.print("checked");%>>Norway</option>
          </select>
        </li>
        <li>
          <label for="city">City:</label>
          <input value="<% if (edit && contact.getCity() != null) {
      out.print(contact.getCity());}%>" class="text_input" type="text" name="city" pattern="^[a-zA-Zа-яА-Я-]{1,30}$"/>
        </li>
        <li>
          <label for="house">Str./House/Apart.</label>
          <input value="<% if (edit && contact.getLocation() != null) {
      out.print(contact.getLocation());}%>" class="text_input" type="text" name="house" pattern="^[a-zA-Zа-яА-Я0-9]{1,7}$"/>
        </li>
        <li id="last_adress_item">
          <label for="zipCode">Zip code:</label>
          <input value="<% if (edit && contact.getZipcode() != null) {
      out.print(contact.getZipcode());}%>" class="text_input" type="text" name="zipCode" pattern="^[0-9]{4,8}$"/>
        </li>
      </ul>

    </li>
    <li id="last_pos">
      <button class="submit" type="submit">OK</button>
    </li>
    <input type="hidden" name="idOfContact" value="<% if (edit) out.print(contact.getPersonId());%>">

  </ul>






  <label>Phones:</label>
  <table id="phone-table">
    <tr>
      <td>

      </td>

      <td>
        Phone number
      </td>

      <td>
        Type
      </td>

      <td>
        Comment
      </td>

      <td>

      </td>

    </tr>

    <%
      if (edit && contact.getPhones() != null) {
        int i = 0;
        for (Phone phone : contact.getPhones()) {
          ++i;
          out.print(String.format("<tr>%n    <td>%n        <a href=\"PleaseEnableJavascript.html\" onclick=\"deleteRowFromPhones(%d);return false;\">x</a>%n   </td>%n", i));
          out.print(String.format("    <td>%n        <input type=\"hidden\" id=\"status%d\" name=\"status%d\" value=\"old\">%n", i, i));
          out.print(String.format("        <input type=\"hidden\" id=\"realID%d\" name=\"realID%d\" value=\"%d\">%n", i, i, phone.getPhoneId()));
          out.print(String.format("        <input type=\"hidden\" id=\"operatorCode%d\" name=\"operatorCode%d\" value=\"%d\">%n", i, i, phone.getOperatorCode()));
          out.print(String.format("        <input type=\"hidden\" id=\"countryCode%d\" name=\"countryCode%d\" value=\"%d\">%n", i, i, phone.getCountryCode()));
          out.print(String.format("        <input type=\"hidden\" id=\"phoneNumber%d\" name=\"phoneNumber%d\" value=\"%d\">%n", i, i, phone.getPhone()));

          out.print(String.format("        <label id=\"phoneLabel%d\">\n", i));
          out.print(String.format("        <a title=\"Click to do something\" href=\"PleaseEnableJavascript.html\" onclick=\"editContact(%d);return false;\">+(%d-%d)%d</a>%n",
                  i, phone.getCountryCode(), phone.getOperatorCode(), phone.getPhone()));
          out.print(String.format("        </label>%n    </td>\n"));
          out.print(String.format("    <td>\n        <input type=\"hidden\" id=\"phoneType%d\" name=\"phoneType%d\" value=\"%s\"><label id=\"phoneTypeLabel%d\">mobile\n        </label>\n    </td>",
                  i, i, phone.getType(), i));
          out.print(String.format("    <td>\n        <input type=\"hidden\" id=\"comment%d\" name=\"comment%d\" value=\"%s\">\n        " +
                          "<label id=\"phoneCommentLabel%d\">%s</label></td>\n</tr>",
                  i, i, phone.getComment(), i, phone.getComment()));

        }
      }
    %>

  </table>

    <label>Files:</label> <br>
    <table id="files-table">
      <tr>
        <td>


        </td>

        <td>
          Name
        </td>

        <td>
          Comment
        </td>


      </tr>

      <%

        if (edit && contact.getFiles() != null) {
          int i = 0;
          for (AttachmentFile file: contact.getFiles()) {
            ++i;
            out.print(String.format("<tr>%n    <td>%n        <a href=\"PleaseEnableJavascript.html\" onclick=\"deleteRowFromFiles(%d);return false;\">x</a>%n   </td>%n", i));
            out.print(String.format("    <td>%n        <input type=\"hidden\" id=\"fileStatus%d\" name=\"fileStatus%d\" value=\"old\">%n", i, i));
            out.print(String.format("        <input type=\"hidden\" id=\"realFileID%d\" name=\"realFileID%d\" value=\"%d\">%n", i, i, file.getFileId()));
            out.print(String.format("        <label id=\"file-label-%d\"><a href=\"\" onclick=\"editFile(%d);return false;\">%s</a></label>%n", i, i, file.getFileUserName()));
            out.print(String.format("        <input name=\"file-user-name-%d\" id=\"file-user-name-%d\" type=\"hidden\" value=\"%s\">%n", i, i, file.getFileUserName()));
            out.print(String.format("        <input name=\"fileComment%d\" id=\"fileComment%d\" type=\"hidden\" value=\"%s\">%n    </td>", i, i, file.getComment()));
            String filePath = "upload" +
                    File.separator + contact.getPersonId() + File.separator + file.getFileName();

            out.print(String.format("    <td>\n        <label id=\"comment-label-%d\"><a href='%s'>%s</a></label></td>\n</tr>",  i, filePath, file.getComment()));
          }
        }
      %>

    </table>

  <input type="hidden" name="countOfFiles" id="numberOfFiles" value="<%
  if (!edit) out.print(0);
  else out.print(contact.getFiles().size());

  %>">
    <input type="hidden" name="countOfPhones" id="numberOfPhone" value="<%
  if (!edit) out.print(0);
  else out.print(contact.getPhones().size());

  %>">
    <input type="hidden" name="phonesToDelete" id="phonesToDelete" value="">
    <input type="hidden" name="filesToDelete" id="filesToDelete" value="">

  <div id="Wrapp" style="display:none">
    <div id='tt'><div class="close" onclick="openbox('Wrapp')">x</div>
      Code of county: <input maxlength="7" type="text" onkeypress="validate(event)" id="countryCode"> <br>
      Code of operator: <input maxlength="7" type="text" onkeypress="validate(event)" id="operatorCode"> <br>
      Phone number: <input maxlength="9" type="text" onkeypress="validate(event)" id="phoneNumber"> <br>
      Type of phone: <input id="r1" type="radio" name="phoneType" value="mobile" checked>Mobile  <input id="r2" type="radio" name="phoneType" value="home">Home <br>
      Comment: <input maxlength="350" type="text" id="comment"> <br>
      <input id="sendButton"  type="button" value="Add" onclick="addPhone()">

    </div></div>

    <div id="Wrapp2" style="display:none">
      <div id='tt2'><div class="close" onclick="openFileBox('Wrapp2')">x</div>
        Name: <input maxlength="45" type="text" id="fileUserName"> <br>


        Comment: <input maxlength="350" type="text" id="fileComment"> <br>
        <table border="0" id="hidden-inputs">

              <%
                if (edit && contact.getFiles() != null) {
                  out.print(String.format("<tr>\n<td>\n<input name=\"%d\" id=\"file-input-%d\" type=\"file\"></td>\n </tr>\n", contact.getFiles().size() + 1, contact.getFiles().size() + 1));
                } else {
                  out.print("<tr>\n<td>\n<input name=\"1\" id=\"file-input-1\" type=\"file\"></td>\n </tr>\n");

                }
              %>




        </table>
        <br>

        <input id="sendFileButton"  type="button" value="Add" onclick="addFile()">

      </div></div>




  <input type="button" onclick="openbox('Wrapp');return false;" value="Add Phone">
    <input type="button" onclick="openFileBox('Wrapp2');return false;" value="Add File">
  </div>
    </td>
      <td width="30%" align="left">
        <div class="image-upload">
          <label for="avatar-file-input">
            <img src="<%
            String uploadUserAvatarPath = null;
            if (edit && contact.getAvatar() != null) {
             uploadUserAvatarPath = "upload" +
             File.separator + contact.getPersonId() + File.separator + "avatar" + File.separator + contact.getAvatar();
             } else {
             uploadUserAvatarPath = "resources/avatar.jpg";
             }
             out.print(uploadUserAvatarPath);%>"/>
          </label>

          <input style="display:none" name="0" id="avatar-file-input" type="file" accept="image/jpeg,image/png,image/gif">
        </div>
      </td>

      <td>

      </td>
    </tr>

  </table>


</form>
