/**
 * Created by antonkw on 02.03.2015.
 */
var fileEditValue = 0;
var numberOfEditFile = 0;
var countOfExistingPhones = 0;
var countOfFiles = 0;
var ifCancel = 1;
var g_calendarObject;


Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 0, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};
function submitEditAdd() {
    if (g_calendarObject.getSelectedDayFormatted() !== false) {
        document.getElementById('dateOfBirth').value = g_calendarObject.getSelectedDayFormatted();

    }

}

function clearFileInput(ctrl) {
    try {
        ctrl.value = null;
    } catch(ex) { }
    if (ctrl.value) {
        ctrl.parentNode.replaceChild(ctrl.cloneNode(true), ctrl);
    }
}

function getFileName(fileID) {
    var filename;
    var fullPath = document.getElementById('file-input-' + fileID).value;
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
         filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }


    }
    return filename;
}

function openFileBox(id,tt) {

    var button = document.getElementById('sendFileButton');
    if (fileEditValue == 0) {
        button.value = "Add";
    } else if (fileEditValue == 1) {
        button.value = "Save";
    }

    var div = document.getElementById(id);
    var tt_div = document.getElementById(tt);
    if(div.style.display == 'block') {
        div.style.display = 'none';
        if (ifCancel == 1) {
            var showFileInputNumber = parseInt(document.getElementById('numberOfFiles').value) + 1;
            if (parseInt(document.getElementById('numberOfFiles').value) != 0) {
                if (numberOfEditFile != 0) {
                    if (document.getElementById('fileStatus' + numberOfEditFile).value == "new") {

                        document.getElementById("file-input-" + numberOfEditFile).style.display = "none";
                    }
                    document.getElementById("file-input-" + showFileInputNumber).style.display = "block";
                }
            }
            document.getElementById('fileComment').value = "";
            document.getElementById('fileUserName').value = "";
            fileEditValue = 0;
            document.getElementById("file-input-" + showFileInputNumber).value = "";


        }
    }
    else {
        div.style.display = 'block';

    }

}

function deleteRowFromFiles(r) {
    if (document.getElementById("files-table").rows[r].cells[1].childNodes[0].value !== "new") {

        document.getElementById("filesToDelete").value = document.getElementById("filesToDelete").value + document.getElementById("files-table").rows[r].cells[1].childNodes[3].value + " ";
    }
    deleteRow(r, "files-table");
    deleteRow(r - 1, "hidden-inputs");
    var myTable = document.getElementById('files-table');
    for (i = 1; i < document.getElementById("files-table").getElementsByTagName("tr").length ; i++) {
        myTable.rows[i].cells[0].innerHTML = "<a href=\"PleaseEnableJavascript.html\" onclick=\"deleteRowFromFiles(" + i + ");return false;\">x<\/a>";
    }



}



function addFile() {
    var numberOfFiles = parseInt(document.getElementById('numberOfFiles').value);
    var fileID = numberOfFiles + 1;
    if (isBlank(document.getElementById('fileComment').value)) {
        alert("write comment");
        return;
    }
    if (isBlank(document.getElementById('fileUserName').value)) {
        alert("write name");
        return;
    }


    if (fileEditValue == 0) {
        var fileTable = document.getElementById('files-table');
        if (isBlank(document.getElementById('file-input-' + fileID).value)) {
            alert("attach your file!");
            return;
        }
        var fileName = getFileName(fileID);


        var comment = document.getElementById('fileComment').value;
        var fileUserName = document.getElementById('fileUserName').value;

        var deleteIndex = document.getElementById("files-table").getElementsByTagName("tr").length;


        var newRow = fileTable.insertRow(document.getElementById("files-table").getElementsByTagName("tr").length);

        var radio = newRow.insertCell(0);



        radio.width = "20";
        radio.innerHTML = "<a href=\"PleaseEnableJavascript.html\" onclick=\"deleteRowFromFiles(" +  deleteIndex + ");return false;\">x<\/a>";


        var fileInputName = newRow.insertCell(1);
        fileInputName.innerHTML = "<input type=\"hidden\" id=\"fileStatus" + fileID + "\" name=\"fileStatus" + fileID + "\" value=\"" + "new" + "\">" + "<label id=\"file-label-" + fileID + "\"><a href=\"\" onclick=\"editFile(" + fileID + ");return false;\">" + fileUserName + "<\/a><\/label>" +
        "<input name=\"file-user-name-" + fileID + "\"id=\"file-user-name-" + fileID + "\" type=\"hidden\" value=\"" + fileUserName + "\">" + "<input name=\"fileComment" + fileID + "\" id=\"fileComment" + fileID + "\" type='hidden' value='" + comment + "' >";
        var commentInputCell = newRow.insertCell(2);
        commentInputCell.innerHTML = "<label id=\"comment-label-" + fileID + "\">" + comment + "<\/label>";
        document.getElementById("file-input-" + fileID).style.display = "none";
        var inputs = document.getElementById('hidden-inputs');
        var newInputRow = inputs.insertRow(document.getElementById("hidden-inputs").getElementsByTagName("tr").length);
        var nextInputNumber = fileID + 1;
        var newInputCell = newInputRow.insertCell(0);
        newInputCell.innerHTML =  "<input name=\"" + nextInputNumber + "\"id=\"file-input-" + nextInputNumber + "\" type=\"file\">";
        document.getElementById('fileComment').value = "";
        document.getElementById('fileUserName').value = "";
        document.getElementById('numberOfFiles').value = fileID;














    }

    if (fileEditValue == 1) {
        var showFileInputNumber = parseInt(document.getElementById('numberOfFiles').value) + 1;

        if (numberOfEditFile == 0) {
            fileEditValue = 0;
            return;
        }

        if (document.getElementById('fileStatus' + numberOfEditFile).value == "old") {
            document.getElementById('fileStatus' + numberOfEditFile).value = "edit";
        }




        if (document.getElementById('fileStatus' + numberOfEditFile).value == "new") {
            if (isBlank(document.getElementById('file-input-' + numberOfEditFile).value)) {
                alert("attach your file!");


                return;
            }
            document.getElementById('file-input-' + showFileInputNumber).style.display = "block";
            document.getElementById('file-input-' + numberOfEditFile).style.display = "none";
        } else {
            document.getElementById("file-input-" + showFileInputNumber).style.display = "block";
        }



        document.getElementById('file-user-name-' + numberOfEditFile).value = document.getElementById('fileUserName').value;
        document.getElementById('fileComment' + numberOfEditFile).value = document.getElementById('fileComment').value;

        document.getElementById("file-label-" + numberOfEditFile).innerHTML = "<a href=\"\" onclick=\"editFile(" + numberOfEditFile + ");return false;\">" + document.getElementById('fileUserName').value + "<\/a>";
        document.getElementById("comment-label-" + numberOfEditFile).innerHTML = document.getElementById('fileComment').value;

        fileEditValue = 0;

        document.getElementById('fileUserName').value = "";
        document.getElementById('fileComment').value = "";

        numberOfEditFile = 0;

    }
    ifCancel = 0;
    openFileBox('Wrapp2');
}

function editFile(numberOfFile) {
    document.getElementById('fileUserName').value = document.getElementById('file-user-name-' + numberOfFile).value;
    document.getElementById('fileComment').value = document.getElementById('fileComment' + numberOfFile).value;
    var showFileInputNumber = parseInt(document.getElementById('numberOfFiles').value) + 1;

    if (document.getElementById('fileStatus' + numberOfFile).value == "new") {
        document.getElementById("file-input-" + showFileInputNumber).style.display = "none";
        document.getElementById("file-input-" + numberOfFile).style.display = "block";
    } else {
        document.getElementById("file-input-" + showFileInputNumber).style.display = "none";

    }




    window.fileEditValue = 1;
    numberOfEditFile = numberOfFile;
    ifCancel = 1;
    openFileBox('Wrapp2');
}
