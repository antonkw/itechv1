/**
 * Created by antonkw on 15.03.2015.
 */
function changeTemplate () {
    var select = document.getElementById("temp");
    if (select.options[0].selected === true) {
        document.getElementById("templateName").value = "empty";
        document.getElementById("mail").readOnly = false;
        document.getElementById("mail").value = "";
        return;

    }
    for (var i = 1; i<select.options.length; i++) {
        if(select.options[i].selected === true) {
            document.getElementById("templateName").value = select.options[i].getAttribute("name");
            document.getElementById("mail").value = document.getElementById("tempN" + i).innerHTML;
            document.getElementById("mail").readOnly = true;
            return;
        }
    }
}
function sendMail () {
    if (isBlank(document.getElementById("mail").value)) {
        alert("mail textarea is empty!");
        return;
    }
    if (isBlank(document.getElementById("theme").value)) {
        alert("mail theme is empty!");
        return;
    }
    sendEMail.submit();
}

