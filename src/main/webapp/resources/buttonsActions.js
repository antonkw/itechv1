/**
 * Created by antonkw on 16.02.2015.
 */

var editValue = 0;
var numberOfEditContact = 0;
var countOfExistingPhones = 0;
var ifPhoneCancel = 1;
function create() {
    document.contacts.buttonName.value = "createButton";
    contacts.submit();
}
function search() {
    var url = "./frontServlet?command=StartSearch";
    //document.location.href(url);
    location.replace(url);
}

function spam() {
    document.contacts.contactsToDelete.value = "";
    for (var i = 1; i < document.getElementById("hor-minimalist-b").getElementsByTagName("tr").length; i++) {
        if (document.getElementById("hor-minimalist-b").rows[i].cells[0].childNodes[1].checked) {

            document.contacts.contactsToDelete.value = document.contacts.contactsToDelete.value + " " + document.getElementById("hor-minimalist-b").rows[i].cells[0].childNodes[1].value;
        }
    }

    if(isBlank(document.contacts.contactsToDelete.value)) {
        alert("nothing checked!");
        return;
    }
    document.contacts.buttonName.value = "spamButton";
    contacts.submit();
}

function deleteContacts() {
    document.contacts.contactsToDelete.value = "";
    for (var i = 1; i < document.getElementById("hor-minimalist-b").getElementsByTagName("tr").length; i++) {
        if (document.getElementById("hor-minimalist-b").rows[i].cells[0].childNodes[1].checked) {

            document.contacts.contactsToDelete.value = document.contacts.contactsToDelete.value + " " + document.getElementById("hor-minimalist-b").rows[i].cells[0].childNodes[1].value;
        }
    }

    if(isBlank(document.contacts.contactsToDelete.value)) {
        alert("nothing checked!");
        return;
    }




    document.contacts.buttonName.value = "deleteButton";
    contacts.submit();
}
function showPage(number) {
    var url = "./frontServlet?command=Show&page=" + number.toString();
    //document.location.href(url);
    location.replace(url);
}

function showPage(number) {
    var url = "./frontServlet?command=Show&page=" + number.toString();
    //document.location.href(url);
    location.replace(url);
}


function showSearchPage(number) {
    document.getElementById('page').value = number;
    //document.location.href(url);
    countPerPage.submit();
}


function getCookie(name) {
    var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function isPageSettingsExist() {
    return getCookie("count") != undefined;
}
function setRadioButtons(number) {
    var radios = document.forms["countPerPage"].elements["count"];

    if (isPageSettingsExist() && getCookie("count") == 20) {
        document.countPerPage.count[1].checked=true;
    }
    else {
        document.countPerPage.count[0].checked=true;
    }

    for (var i = 0, max = radios.length; i < max; i++) {
        radios[i].onclick = function() {
            var d = new Date();
            d.setTime(d.getTime() + (15*24*60*60*1000));
            if (document.countPerPage.count[0].checked == true) {
                number = number*2 - 1;
                document.cookie="count=10; path=/; expires=" + d.toUTCString();
            }
            else {
                number = Math.ceil(number/2);
                document.cookie="count=20; path=/; expires=" + d.toUTCString();
            }
            showPage(number);

        }
    }

}

function setRadioButtonsForSearch(number) {
    var radios = document.forms["countPerPage"].elements["count"];

    if (isPageSettingsExist() && getCookie("count") == 20) {
        document.countPerPage.count[1].checked=true;
    }
    else {
        document.countPerPage.count[0].checked=true;
    }

    for (var i = 0, max = radios.length; i < max; i++) {
        radios[i].onclick = function() {
            var d = new Date();
            d.setTime(d.getTime() + (15*24*60*60*1000));
            if (document.countPerPage.count[0].checked == true) {
                number = number*2 - 1;
                document.cookie="count=10; path=/; expires=" + d.toUTCString();
            }
            else {
                number = Math.ceil(number/2);
                document.cookie="count=20; path=/; expires=" + d.toUTCString();
            }
            showSearchPage(number);

        }
    }

}

function editContact(numberOfContact) {
    document.getElementById('phoneNumber').value = document.getElementById('phoneNumber' + numberOfContact).value;
    document.getElementById('countryCode').value = document.getElementById('countryCode' + numberOfContact).value;
    document.getElementById('operatorCode').value = document.getElementById('operatorCode' + numberOfContact).value;
    document.getElementById("comment").value = document.getElementById("comment" + numberOfContact).value;

    if (document.getElementById("phoneType" + numberOfContact).value == "mobile") {
        var radiobtn = document.getElementById("r1");
        radiobtn.checked = true;
    }
    else {
        radiobtn = document.getElementById("r2");
        radiobtn.checked = true;
    }
    window.editValue = 1;
    numberOfEditContact = numberOfContact;
    ifPhoneCancel = 1;
    openbox('Wrapp');
}



function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

function openbox(id,tt) {
    var button = document.getElementById('sendButton');
    if (editValue == 0) {
        button.value = "Add";
    } else if (editValue == 1) {
        button.value = "Save";
    }
    var div = document.getElementById(id);
    var tt_div = document.getElementById(tt);
    if(div.style.display == 'block') {
        div.style.display = 'none';
        if (ifPhoneCancel == 1) {
            document.getElementById('phoneNumber').value = "";
            document.getElementById('countryCode').value = "";
            document.getElementById('operatorCode').value = "";
            document.getElementById("comment").value = "";
            var radiobtn = document.getElementById("r1");
            radiobtn.checked = true;
            editValue = 0;

        }
    }
    else {
        div.style.display = 'block';
    }
}




function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function deleteRow(r, table) {

    document.getElementById(table).deleteRow(r);
    countOfExistingPhones--;
}

function deleteRowFromPhones(r) {
    if (document.getElementById("phone-table").rows[r].cells[1].childNodes[1].value != "new") {
        document.getElementById("phonesToDelete").value = document.getElementById("phonesToDelete").value+ document.getElementById("phone-table").rows[r].cells[1].childNodes[3].value  + " " ;
    }
    deleteRow(r, "phone-table");
    var myTable = document.getElementById('phone-table');
    for (i = 1; i <= countOfExistingPhones; i++) {
        myTable.rows[i].cells[0].innerHTML = "<a href=\"PleaseEnableJavascript.html\" onclick=\"deleteRowFromPhones(" + i + ");return false;\">x<\/a>";
    }

}

function addPhone() {

    if (editValue == 0) {
        var phoneTable = document.getElementById('phone-table');
        var phoneNumber = document.getElementById('phoneNumber').value;
        var countryCode = document.getElementById('countryCode').value;
        var operatorCode = document.getElementById('operatorCode').value;
        var comment = document.getElementById("comment").value;
        if (isBlank(phoneNumber)) {
            alert("Enter phone number!");
            return;
        }
        if (isBlank(countryCode)) {
            alert("Enter country code.");
            return;
        }
        if (isBlank(operatorCode)) {
            alert("Enter operator code. You can not specifying it if the country code is missing");
            return;
        }
        if (isBlank(comment)) {
            alert("Enter operator code. You can not specifying it if the country code is missing");
            return;
        }
        var numberOfPhone = parseInt(document.getElementById('numberOfPhone').value);
        var phoneID = numberOfPhone + 1;
        countOfExistingPhones++;
        var newRow = phoneTable.insertRow(document.getElementById("phone-table").getElementsByTagName("tr").length);

        var radio = newRow.insertCell(0);

        var phoneType;
        if (document.getElementById("r1").checked) {
            phoneType = "mobile";
        }
        else {
            phoneType = "home";
        }
        document.getElementById('numberOfPhone').value = phoneID;
        radio.width = "20";
        radio.innerHTML = "<a href=\"PleaseEnableJavascript.html\" onclick=\"deleteRowFromPhones(" + countOfExistingPhones + ");return false;\">x<\/a>";

        var fullNumber;
        fullNumber = "+(" + countryCode + "-" + operatorCode + ")" + phoneNumber;

        var phoneCell = newRow.insertCell(1);
        phoneCell.innerHTML = "<input type=\"hidden\" id=\"status" + phoneID + "\" name=\"status" + phoneID + "\" value=\"new\">" +
        "<input type=\"hidden\" id=\"operatorCode" + phoneID + "\" name=\"operatorCode" + phoneID + "\" value=\"" + operatorCode  + "\">" +
        "<input type=\"hidden\" id=\"countryCode" + phoneID + "\" name=\"countryCode" + phoneID + "\" value=\"" + countryCode + "\">" +
        "<input type=\"hidden\" id=\"phoneNumber" + phoneID + "\" name=\"phoneNumber" + phoneID + "\" value=\"" + phoneNumber + "\">" + "<label id=\"phoneLabel" + phoneID + "\"> <a title=\"Click to do something\"" +
        "href=\"PleaseEnableJavascript.html\" onclick=\"editContact(" + phoneID + ");return false;\">" + fullNumber + "</a></label>";
        var phoneTypeCell = newRow.insertCell(2);
        phoneTypeCell.innerHTML = "<input type=\"hidden\" id=\"phoneType" + phoneID + "\" name=\"phoneType" + phoneID + "\" value=\"" + phoneType + "\">"
        + "<label id=\"phoneTypeLabel" + phoneID + "\">" + phoneType + "</label>";
        var commentCell = newRow.insertCell(3);
        commentCell.innerHTML = "<input type=\"hidden\" id=\"comment" + phoneID + "\" name=\"comment" + phoneID + "\" value=\"" + comment+ "\">" +
        "<label id=\"phoneCommentLabel" + phoneID +"\">" + comment + "</label>";
        openbox('Wrapp');
        document.getElementById('phoneNumber').value = "";
        document.getElementById('countryCode').value = "";
        document.getElementById('operatorCode').value = "";
        document.getElementById("comment").value = "";
        var radiobtn = document.getElementById("r1");
        radiobtn.checked = true;
    }

    if (editValue == 1) {
        if (numberOfEditContact == 0) {
            editValue = 0;
            return;
        }
        if (document.getElementById('status' + numberOfEditContact).value == "old") {
            document.getElementById('status' + numberOfEditContact).value = "edit";
        }
        //проверять по статусу statusx, если если не new, то менять на edit


        document.getElementById('phoneNumber' + numberOfEditContact).value = document.getElementById('phoneNumber').value;
        document.getElementById('countryCode' + numberOfEditContact).value = document.getElementById('countryCode').value;
        document.getElementById('operatorCode' + numberOfEditContact).value = document.getElementById('operatorCode').value;
        document.getElementById("comment" + numberOfEditContact).value = document.getElementById("comment").value;
        if (document.getElementById("r1").checked) {
            document.getElementById('phoneType' + numberOfEditContact).value = "mobile";
        }
        else {
            document.getElementById('phoneType' + numberOfEditContact).value = "home";
        }

        document.getElementById("phoneLabel" + numberOfEditContact).innerHTML = "<a title=\"Click to do something\"" +
        "href=\"PleaseEnableJavascript.html\" onclick=\"editContact(" + numberOfEditContact + ");return false;\">" + "+(" + document.getElementById('countryCode').value +
        "-" + document.getElementById('operatorCode').value + ")" + document.getElementById('phoneNumber').value + "<\/a>" ;
        document.getElementById("phoneTypeLabel" + numberOfEditContact).innerHTML = document.getElementById('phoneType' + numberOfEditContact).value;
        document.getElementById("phoneCommentLabel" + numberOfEditContact).innerHTML = document.getElementById('comment' + numberOfEditContact).value;
        if (document.getElementById("status" + numberOfEditContact).value == "old") {
            document.getElementById("status" + numberOfEditContact).value = "edit";
        }

        editValue = 0;
        openbox('Wrapp');
        document.getElementById('phoneNumber').value = "";
        document.getElementById('countryCode').value = "";
        document.getElementById('operatorCode').value = "";
        document.getElementById("comment").value = "";
        var radiobtn = document.getElementById("r1");
        radiobtn.checked = true;
        numberOfEditContact = 0;
    }
    ifPhoneCancel = 0;
}
