<%--
  Created by IntelliJ IDEA.
  User: antonkw
  Date: 11.03.2015
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Contact.SearchRequest" %>
<%@ page import="org.apache.logging.log4j.LogManager" %>
<%@ page import="org.apache.logging.log4j.Logger" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page import="org.joda.time.format.DateTimeFormatter" %>
<%
  Logger log = LogManager.getLogger(this.getClass().getName());
%>

<html>
<head>
  <title>Contacts</title>
</head>
<%
  Integer pageNumber = null;
  Integer countOfPages = null;
  SearchRequest searchRequest = null;
  try {
    pageNumber = (Integer) request.getAttribute("pageNumber");
    countOfPages = (Integer) request.getAttribute("countOfPages");
    searchRequest = (SearchRequest) request.getAttribute("searchRequest");

    request.setAttribute("searchRequest", searchRequest);

  } catch (Exception e) {
    out.print("page number reading error");
    log.error(e);

  }


%>
<body onload="setRadioButtonsForSearch(<%=pageNumber%>)">
<jsp:include page="showTable"/>
<table width="1000px" border="0">


  <tr>
    <td width="33%">

    </td>
    <td width="33%">

      <table border="0">
        <tr>
          <td width="33%" align="right">
            <%
              if (pageNumber != 1) {
                int prevPage = pageNumber - 1;
                out.println("<input type=\"button\" class=\"myButton\" onclick=\"showSearchPage(" + prevPage + ")\" value=\"Previous\">");
              }
            %>
          </td>
          <td width="33%" align="center">
            <%

              out.println(pageNumber + "of" + countOfPages);

            %>


          </td>
          <td>
            <%
              if (pageNumber != countOfPages) {
                int nextPage = pageNumber + 1;
                out.println("<input type=\"button\" class=\"myButton\" onclick=\"showSearchPage(" + nextPage + ")\" value=\"Next\">");
              }
            %>

          </td>


        </tr>

      </table>


    </td>
    <td align="right">
      <form method="post" action="frontServlet?command=Search" name="countPerPage">
        <input type="hidden" value="" name="page" id="page">
        <input type="hidden" name="command" value="Search">
        <input type="hidden" value="<% if(searchRequest.getName() !=null ) out.print(searchRequest.getName());%>searchRequest.getName()%>" name="name"/>
        <input type="hidden" value="<% if(searchRequest.getLastName() != null) out.print(searchRequest.getLastName());%>" name="patronymic">
        <input type="hidden" value="<% if(searchRequest.getDateTime() != null) { DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                      out.print( formatter.print(searchRequest.getDateTime())); }%>" name="dateOfBirth">
        <input type="hidden" value="<%=searchRequest.isLater()%>" name="date">

        <input type="hidden" value="<% if (searchRequest.getCountryOfCitizenship() != null) out.print(searchRequest.getCountryOfCitizenship());%>" name="citizenship">
        <input type="hidden" value="<% if (searchRequest.getSex() != null) out.print(searchRequest.getSex());%>" name="sex">
        <input type="hidden" value="<% if (searchRequest.getMaritalStatus() != null) out.print(searchRequest.getMaritalStatus());%>" name="maritalStatus">
      </form>


    </td>
  </tr>
</table>

<form action="actionFilter" name="contacts" method="POST">
  <br>
  <table border="0px" width="1000px" align="left">
    <tr align="center">
      <td>
        <input type="button" class="myButton" onclick="create()" value="Create">
      </td>
      <td>
        <input type="button" class="myButton" onclick="deleteContacts()"value="Delete">
      </td>
      <td>
        <input type="button" class="myButton" onclick="search()" value="Search">
      </td>
      <td>
        <input type="button" class="myButton" onclick="spam()" value="Spam">
      </td>
    </tr>
  </table>
  <input type="hidden" name="buttonName">
  <input type="hidden" name="contactsToDelete">


</form>


</body>
<link rel="stylesheet" media="screen" href="resources/styles.css">
<script src="./resources/buttonsActions.js" type="text/javascript"></script>
</html>
