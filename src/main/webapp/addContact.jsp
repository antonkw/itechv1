<%@ page import="DBService.JDBCContactsDAO" %>
<%--
  Created by IntelliJ IDEA.
  User: antonkw
  Date: 16.02.2015
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Create new contact!</title>
    <link rel="stylesheet" media="screen" href="resources/styles.css" >
    <link rel="stylesheet" type="text/css" media="all" href="resources/jsDatePick_ltr.min.css">
    <script src="./resources/buttonsActions.js" type="text/javascript"></script>
    <script src="./resources/addFileActions.js" type="text/javascript"></script>
    <script type="text/javascript" src="resources/jsDatePick.min.1.3.js"></script>
    <script type="text/javascript" src="resources/modernizr-2.5.3.js"></script>
    <script type="text/javascript" data-webforms2-support="validation,placeholder"  src="resources/html5Forms.js" > </script>

    <script type="text/javascript">

        window.onload = function(){
            g_calendarObject = new JsDatePick({
                useMode:2,
                target:"inputField",
                dateFormat:"%Y-%m-%d"
            });
        };
    </script>

</head>
<body>

<%
   request.setAttribute("context", "Add new contact");

%>

<jsp:include page="editContactForm.jsp"/>

</body>
</html>
