<%@ page import="DBService.JDBCContactsDAO" %>
<%@ page import="Mail.AdminProperties" %>
<%@ page import="org.antlr.stringtemplate.language.DefaultTemplateLexer" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.stringtemplate.v4.ST" %>
<%@ page import="org.stringtemplate.v4.STGroup" %>
<%@ page import="org.stringtemplate.v4.STGroupFile" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="Contact.*" %>
<%@ page import="org.antlr.stringtemplate.StringTemplate" %>
<%@ page import="org.apache.logging.log4j.Logger" %>
<%@ page import="org.apache.logging.log4j.LogManager" %>

<%--
  Created by IntelliJ IDEA.
  User: antonkw
  Date: 12.03.2015
  Time: 0:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  Logger log = LogManager.getLogger(this.getClass().getName());
%>
<html>
<head>
    <title></title>
</head>
<body>
<%

  if (StringUtils.isEmpty((String) request.getAttribute("contactsToSpam"))) {
  out.print("you didn`t checked anything!");
  return;
}
  StringBuilder addresses = new StringBuilder();
  String contactsIds = (String) request.getAttribute("contactsToSpam");


  StringTokenizer st = new StringTokenizer(contactsIds);

  List<Integer> contactsToSpam = new ArrayList<Integer>();
  JDBCContactsDAO jdbcContactsDAO = new JDBCContactsDAO();
  STGroup spam = new STGroupFile("spam.stg", '$', '$');
  ST spamTemlate1 = spam.getInstanceOf("spam1");
  spamTemlate1.add("username", "user");
  spamTemlate1.add("admin", AdminProperties.getInstance().getName());
  spamTemlate1.add("mail", AdminProperties.getInstance().getMail());



  ST spamTemlate2 = spam.getInstanceOf("spam2");
  spamTemlate2.add("username", "username");
  spamTemlate2.add("mail", AdminProperties.getInstance().getMail());



  out.print(spamTemlate2.render());
  while (st.hasMoreTokens()) {
    contactsToSpam.add(Integer.parseInt(st.nextToken()));
  }
  List <Contact> contacts = null;
  try {
    contacts = jdbcContactsDAO.getContactsWithMail(contactsToSpam);
    session.setAttribute("mails", contacts);

    for (Contact contact: contacts){
      addresses.append(contact.getName());
      addresses.append("(");
      addresses.append(contact.getEmail());
      addresses.append(") ");
    }
  } catch (Exception e) {

  }
  finally {

  }


%>

  <label id="tempN1" style="display: none"> <%
      try {
        out.print(spamTemlate1.render());
      } catch (Exception e) {
        log.error("first template render error", e);
      }
    %></label>
  <label id="tempN2" style="display: none"> <%
    try {
      out.print(spamTemlate2.render());
    } catch (Exception e) {
      log.error("second template render error", e);
    }
  %></label>
  <br>




  <select id="temp" onchange="changeTemplate()">
    <option name="empty">

    </option>
    <option name="phones">
      Реклама роллета
    </option>
    <option  name="version">
      Оповещение об обновлении
    </option>

  </select>
<form name="sendEMail" method="post" action="frontServlet?command=Mail">
  <label>Adress:</label><br>
  <input readonly size="50%" name="address" value="<%= addresses.toString() %>" type="text"> <br>
  <label>Theme:</label><br>
  <input id="theme" name="theme" type="text">
  <input id="templateName" value="empty" name="templateName" type="hidden">
  <br>
  <label for="mail">Mail</label><br>
  <textarea cols="100" rows="7" name="mail" id="mail"></textarea>
  <br>
  </textarea>
  <input type="hidden" name="command">
  <input type="button" value="Send" onclick="sendMail()">
</form>

</body>
<script type="text/javascript" src="resources/mailActions.js"></script>
<script type="text/javascript" src="resources/buttonsActions.js"></script>
</html>
