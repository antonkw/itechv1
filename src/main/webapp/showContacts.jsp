<%--
  Created by IntelliJ IDEA.
  User: antonkw
  Date: 15.02.2015
  Time: 18:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<html>
<head>
    <title>Contacts</title>
</head>
<%
    Integer pageNumber = null;
    Integer countOfPages = null;
    try {
        pageNumber = (Integer) request.getAttribute("pageNumber");
        countOfPages = (Integer) request.getAttribute("countOfPages");

    } catch (Exception e) {
        out.print("page number reading error");
    }


%>
<body onload="setRadioButtons(<%=pageNumber%>)">
<jsp:include page="showTable"/>
<table width="1000px" border="0">


    <tr>
        <td width="33%">

        </td>
        <td width="33%">

            <table border="0">
                <tr>
                    <td width="33%" align="right">
                        <%
                            try {
                                if (pageNumber != 1) {
                                    int prevPage = pageNumber - 1;
                                    out.println("<input type=\"button\" class=\"myButton\" onclick=\"showPage(" + prevPage + ")\" value=\"Previous\">");
                                }

                            } catch (NullPointerException e) {
                                e.getMessage();
                            }


                        %>
                    </td>
                    <td width="33%" align="center">
                        <%

                            out.println(pageNumber + "of" + countOfPages);

                        %>


                    </td>
                    <td>
                        <%
                            if (pageNumber != countOfPages) {
                                int nextPage = pageNumber + 1;
                                out.println("<input type=\"button\" class=\"myButton\" onclick=\"showPage(" + nextPage + ")\" value=\"Next\">");
                            }
                        %>

                    </td>


                </tr>

            </table>


        </td>
        <td align="right">
            <form name="countPerPage">
                <input type="radio" name="count" value="10">10
                <input type="radio" name="count" value="20">20
            </form>


        </td>
    </tr>
</table>

<form action="actionFilter" name="contacts" method="POST">
    <br>
    <table border="0px" width="1000px" align="left">
        <tr align="center">
            <td>
                <input type="button" class="myButton" onclick="create()" value="Create">
            </td>
            <td>
                <input type="button" class="myButton" onclick="deleteContacts()"value="Delete">
            </td>
            <td>
                <input type="button" class="myButton" onclick="search()" value="Search">
            </td>
            <td>
                <input type="button" class="myButton" onclick="spam()" value="Spam">
            </td>
        </tr>
    </table>
    <input type="hidden" name="buttonName">
    <input type="hidden" name="contactsToDelete">


</form>


</body>
<link rel="stylesheet" media="screen" href="./resources/styles.css">
<script src="./resources/buttonsActions.js" type="text/javascript"></script>
</html>
