<%--
  Created by IntelliJ IDEA.
  User: antonkw
  Date: 10.03.2015
  Time: 23:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search</title>
  <link rel="stylesheet" media="screen" href="resources/styles.css" >
  <link rel="stylesheet" type="text/css" media="all" href="resources/jsDatePick_ltr.min.css">
  <script src="./resources/buttonsActions.js" type="text/javascript"></script>
  <script src="./resources/addFileActions.js" type="text/javascript"></script>
  <script type="text/javascript" src="resources/jsDatePick.min.1.3.js"></script>
  <script type="text/javascript">

    window.onload = function(){
      g_calendarObject = new JsDatePick({
        useMode:2,
        target:"inputField",
        dateFormat:"%Y-%m-%d"
      });
    };
  </script>

</head>
<body>
<form  accept-charset="UTF-8" class="contact_form" action="frontServlet?command=Search" method="post" name="contact_form">





        <div>
          <ul>
            <li id="first_pos">
              <h2>Search</h2>
              <span class="required_notification">* Denotes Required Field</span>
            </li>
            <li>
              <label for="name">Name:</label>
              <input value="" class="text_input" type="text" name="name" pattern="^[a-zA-Zа-яА-Я][a-zа-я]{1,20}$"/>
            </li>
            <li>
              <label for="lastName">Last Name:</label>
              <input value="" class="text_input" type="text" name="lastName" pattern="^[a-zA-Zа-яА-Я][a-zа-я]{1,20}$"/>
            </li>
            <li>
              <label for="patronymic">Patronymic:</label>
              <input value="" class="text_input" type="text" name="patronymic" pattern="^[a-zA-Zа-яА-Я][a-zа-я]{1,20}$"/>
            </li>
            <li>
                <input type="radio" name="date" value="before" checked>Before
                <input type="radio" name="date" value="after">After
              <label for="dateOfBirth">Date of birth:</label>
      <span style="position: relative;"><input onfocusout="g_calendarObject.getSelectedDayFormatted()" class="text_input" type="text" value="" size="12" id="inputField" globalnumber="495"><div class="JsDatePickBox" globalcalnumber="495" style="z-index: 3; position: absolute; top: 18px; left: 0px; display: none;"><div class="boxLeftWall"><div class="leftTopCorner"></div><div class="leftWall" style="height: 231px;"></div><div class="leftBottomCorner"></div></div><div class="boxMain"><div class="boxMainInner"><div class="controlsBar" globalnumber="495"><div class="monthForwardButton" globalnumber="495"></div><div class="monthBackwardButton" globalnumber="495"></div><div class="yearForwardButton" globalnumber="495"></div><div class="yearBackwardButton" globalnumber="495"></div><div class="controlsBarText">March, 2015</div></div><div class="clearfix"></div><div class="tooltip"></div><div class="weekDaysRow"><div class="weekDay">Mon</div><div class="weekDay">Tue</div><div class="weekDay">Wed</div><div class="weekDay">Thu</div><div class="weekDay">Fri</div><div class="weekDay">Sat</div><div class="weekDay" style="margin-right: 0px;">Sun</div></div><div class="boxMainCellsContainer"><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div class="skipDay"></div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">1</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">2</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">3</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">4</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">5</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">6</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">7</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">8</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">9</div><div globalnumber="495" istoday="1" class="dayNormalToday" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">10</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">11</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">12</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">13</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">14</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">15</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">16</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">17</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">18</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">19</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">20</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">21</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">22</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">23</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">24</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">25</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">26</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">27</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">28</div><div globalnumber="495" class="dayNormal" style="margin-right: 0px; background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">29</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">30</div><div globalnumber="495" class="dayNormal" style="background: url(file:///W:/iTechArt/Individual%20project/firstVersion/web/resources/img/ocean_blue_dayNormal.gif) 0% 0% no-repeat;">31</div></div><div class="clearfix"></div></div></div><div class="boxRightWall"><div class="rightTopCorner"></div><div class="rightWall" style="height: 231px;"></div><div class="rightBottomCorner"></div></div><div class="clearfix"></div><div class="jsDatePickCloseButton" globalnumber="495"></div><div class="topWall"></div><div class="bottomWall"></div></div></span>
              <input value=""  type="hidden" id="dateOfBirth" name="dateOfBirth" />
            </li>

            <li>
              <label for="citizenship">Country of citizenship:</label>
              <input value="" class="text_input" type="text" name="citizenship" pattern="^[a-zA-Zа-яА-Я\s]{1,20}$"/>
            </li>
            <li>
              <label for="sex">Sex:</label>
              <input type="radio" name="sex" value="male" checked>Male
              <input type="radio" name="sex" value="female">Female
            </li>
            <li>
              <label for="maritalStatus">Marital status:</label>
              <input type="radio" name="maritalStatus" value="unmarried">Unmarried
              <input type="radio" name="maritalStatus" value="married">Married
              <input type="radio" name="maritalStatus" value="divorced">Divorced
              <input type="radio" name="maritalStatus" value="widowed" >Widowed
            </li>

            <li>
              <label for="work">Current place of work:</label>
              <input value="" class="text_input" type="text" name="work" pattern="^[a-zA-Zа-яА-Я][a-zа-я\s]{1,20}$"/>
            </li>
            <li>
              <label>Adress:</label> <br>
              <ul id="adressList">
                <li>
                  <label for="country">Country:</label>
                  <input value="" class="text_input" type="text" name="country" pattern="^[a-zA-Zа-яА-Я\s-]{1,30}$"/>
                </li>
                <li>
                  <label for="city">City:</label>
                  <input value="" class="text_input" type="text" name="city" pattern="^[a-zA-Zа-яА-Я-]{1,30}$"/>
                </li>
              </ul>



            <li id="last_pos">
              <button class="submit" type="submit">Search</button>
            </li>
            <input type="hidden" name="idOfContact" value="">



            </ul>
          </div>
  <input value="Search" type="hidden" name="command"/>
          </form>

</body>
</html>
